# KLOCGA - Logo / tile data and scripts for conversion 

This directory contains the tile and logo graphics for the KLOCGA game, as well as the scripts for converting the data int othe format used in the source code of the program (C arrays). The graphics drawn in GIMP, the XCF files for editing the graphics are provided here (tileset_v10.xcf, logo_v10.xcf).

## Steps for modifying the graphics in the game sources

After editing the tileset (tileset_v10.xcf) or the logo (logo_v10.xcf) file in GIMP, the image must be exported as an indexed-color Windows BMP image. By default, it will use 4 bits per pixel, the format supported by the conversion script. It is important to check the "Do not write color space information" checkbox, otherwise the scripts cannot process the exported BMP file.

Run the conversion script/scripts from command line:

    python3 conv2bpp_tiles.py tileset_v10.bmp > tileset_v10.out
    python3 conv2bpp_logo.py logo_v10.bmp > logo_v10.out

klctiles.c must be updated manually using the data written to the output file/files.

## Licesne

All files in this directory are available under the 2-caluse BSD license.
