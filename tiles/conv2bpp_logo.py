#!/usr/bin/python3
# coding: utf8

# KLOCGA "retro" game, Conversion script for the game logo

# Copyright (c) 2022 knt47
#
# Redistribution and use in source and binary forms, with or without 
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Usage: python3 conv2bpp_logo.py logo_v10.bmp > logo_v10.out
# Note: klctiles.c must be updated manually.

from __future__ import division, print_function, unicode_literals

import itertools, sys

if sys.version_info[0] != 3:
    raise NotImplementedError('Only Python 3 is supported.')
else:
    izip = zip
    xrange = range

import struct
from collections import namedtuple

# Windows BMP header format
# See https://en.wikipedia.org/wiki/BMP_file_format for more information.
# NOTE: This porgram supports only BMP files with "no color spece information"
#   (GIMP) or "BMP3" (ImageMagick)

nt_bm_header = namedtuple('nt_bm_header', (
    'bfType', 'bfSize', 'bfReserved1', 'bfReserved2', 'bfOffBits',
    'biSize', 'biWidth', 'biHeight', 'biPlanes', 'biBitCount', 
    'biCompression', 'biSizeImage', 'biXPelsPerMeter', 'biYPelsPerMeter',
    'biClrUsed', 'biClrImportant'))
    
# Used for decoding a BMP header with struct.unpack
bm_header_struct_format = '<HLHHLLllHHLLllLL'

str_err_bm_read = 'Bitmap file read error.'
str_err_bm_format = 'Unsupported bitmap file type.'

bmp_magic_number = 0x4d42

# Supported values for height_width
bm_height_supported = 52
bm_width_max = 1024

# NOTE: This is value is valid only for 4BPP images using only 4 colors!
#    (at least when the image was exported from GIMP 2.10.18 Linux)
palette_size = 16
# Width of a single tile in the input file, in bytes, must be divisible by 2
# Note: only a single tile
# Note: two black bytes at end
input_tile_width_bytes = 24
# Add line separator after this number of bytes in output
#  11: one row per line
max_bytes_in_line_output = 12

def conv_4bpp_to_2bpp(in_bytes):
    '''
    Converts 4BPP pixel data (the image format used by GIMP for the CGA
        images) to 2BPP (CGA) format.
    in_bytes: Input data,  must be data from a 4BPP image using only 4 colors
        (type: bytearray for Py 2 compatibility)
    Return type: bytearray.
    '''
    
    return bytearray(
        ((in_bytes[x] & 0xf0) << 2) | ((in_bytes[x] & 0xf) << 4) |
        ((in_bytes[x + 1] & 0xf0) >> 2) | (in_bytes[x + 1] & 0xf) 
        for x in xrange(0, len(in_bytes), 2))



def process_bm_4bpp_file(in_f_name):
    '''
    Processes the BMP file with name passed as argument (in_f_name, type: str)
    Prints the output data to the standard output.
    '''

    with open(in_f_name, 'rb') as in_f:
        bm_data = in_f.read()

    head_size = struct.calcsize(bm_header_struct_format)

    if len(bm_data) < head_size:
        raise RuntimeError(str_err_bm_read)
        
    bm_head = nt_bm_header._make(struct.unpack(
        bm_header_struct_format, bm_data[:head_size]))
    
    if ((bm_head.bfType != bmp_magic_number) or 
            (bm_head.biPlanes != 1)):
        raise RuntimeError(str_err_bm_format)
    if bm_head.biCompression != 0:
        raise RuntimeError('Only uncompressed bitmaps are supported.')
    
    if bm_head.biBitCount != 4:
        raise RuntimeError('Only 4BPP bitmaps are supproted.')
    
    if bm_head.biHeight != bm_height_supported:
        raise RuntimeError('This script supports only %d px high files.' %
            bm_height_supported)
    
    if bm_head.biWidth > bm_width_max:
        raise RuntimeError('Unsupported image width')
    
    num_bits_per_row = (bm_head.biWidth + 7) & ~7
    num_bytes_per_row = num_bits_per_row >> 1
    num_bytes_image = num_bytes_per_row * bm_height_supported
    img_data_size = len(bm_data)
    # TODO, Python 2.X compatibility
    img_data_b_array = bytes(bm_data)  
    
    # Note: Ignores palette, considers color #0 as black and #1 as white
    
    if img_data_size < (head_size + palette_size + num_bytes_image):
        # TODO, more sophisticated check
        raise RuntimeError('Bad BMP file size')  
            
    # Currently, the processing simply starts at the end of the image
    # Note: the top row is at the bottom of the file
    
    for pos_x in xrange(img_data_size - num_bytes_per_row, 
            img_data_size, input_tile_width_bytes):
        
        #  print('(', end=' ')
        data_row_out = bytearray()
        
        
        for row_offs in xrange(0, num_bytes_image, num_bytes_per_row):
            data_row_out.extend(
                conv_4bpp_to_2bpp(img_data_b_array[pos_x - row_offs :
                pos_x - row_offs + input_tile_width_bytes]))
            
        print('    ', end='')
        print(', '.join('%s0x%02x' % (
            '\n    ' if x and not (x % max_bytes_in_line_output) else '', y)
            for x, y in enumerate(data_row_out)), end = ',\n')
            
        #  print(' ),')
            
if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit('1 argument needed: in_bmp_file')
        
    process_bm_4bpp_file(sys.argv[1])
