// KLOCGA program, by knt47

// KLCMAIN0.C: Main module, also contains the main menu code

/*
  Copyright (c) 2022 knt47

  Redistribution and use in source and binary forms, with or without 
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <i86.h>

#include "klchead.h"

#define MENU_NUM_ITEMS_ON_SCREEN 16
#define MENU_ITEM_DISTANCE 12
#define MENU_POS_X_BYTES 20
#define MENU_POS_Y 6
#define MENU_SCR_POS_START \
    (MENU_POS_Y * SCREEN_WIDTH_BYTES + MENU_POS_X_BYTES)

#define REL_POS_X_BULLET 3
#define REL_POS_X_BULLET_TEXT 6
#define MENU_ITEM_COLORS_NORMAL 0x300
#define MENU_ITEM_COLORS_SELECTED 0x100
    
#define MENU_MIDDLE_AREA_BYTES 44
#define MENU_CLEAR_WIDTH (MENU_MIDDLE_AREA_BYTES - 4)
#define MENU_CLEAR_HEIGHT \
    (MENU_NUM_ITEMS_ON_SCREEN * MENU_NUM_ITEMS_ON_SCREEN - 4)
    
void draw_menu_item(game_state * state, int item_index, ushort scr_offset)
{
    static const char bullet_char = 0x7f;
    
    if (game_menu_levels[item_index].high_scores_index != MENU_GROUP)
    {
        draw_text_6x8(state->video_buf, &bullet_char, 
            scr_offset + REL_POS_X_BULLET, 1, MENU_ITEM_COLORS_NORMAL);
        draw_text_6x8(
            state->video_buf, game_menu_levels[item_index].level_name,
            scr_offset + REL_POS_X_BULLET_TEXT, DRAW_TEXT_MAX_LEN,
            (item_index == state->menu_selected_item) ? 
                MENU_ITEM_COLORS_SELECTED : MENU_ITEM_COLORS_NORMAL);
    } else
    {
        draw_text_6x8(
            state->video_buf, game_menu_levels[item_index].level_name,
            scr_offset, DRAW_TEXT_MAX_LEN, MENU_ITEM_COLORS_NORMAL);
    }
}

void draw_full_menu(game_state * state)
{
    int item_i_scr;
    int menu_top_line_pos = state->menu_top_line_pos;
    ushort scr_offset = MENU_SCR_POS_START;
    
    // Protection for reading at an illegal array index
    if (menu_top_line_pos < 0)
        return;
    
    for (item_i_scr = 0; item_i_scr < MENU_NUM_ITEMS_ON_SCREEN; ++item_i_scr)
    {
        // Protection for reading at an illegal array index
        if ((item_i_scr + menu_top_line_pos) >= NUM_MAIN_MENU_ITEMS)
            return;
        
        draw_menu_item(state, item_i_scr + menu_top_line_pos, scr_offset);
        scr_offset += MENU_ITEM_DISTANCE * SCREEN_WIDTH_BYTES;
    }
}

void menu_move_up(game_state * state, int num_steps)
{
    // Returns the selected menu item after the move
    int menu_i;
    int menu_sel_start = state->menu_selected_item;
    int new_sel = menu_sel_start;
    int count = 0;
    int menu_top_line_pos = state->menu_top_line_pos;
    ushort scr_offset;
    
    if (num_steps <= 0)
        return;
    
    for (menu_i = menu_sel_start - 1; menu_i >= 0; --menu_i)
    {
        if (game_menu_levels[menu_i].high_scores_index != MENU_GROUP)
        {
            new_sel = menu_i;
            
            if (++count == num_steps)
                break;
        }
    }
    
    if (new_sel == menu_sel_start)
        return;
    
    state->menu_selected_item = new_sel;
    
    // Hack to make top group text visible again 
    if ((new_sel >= menu_top_line_pos) &&
            (new_sel > FIRST_SELECTABLE_MENU_ITEM))
    {
        // FIXME? code duplication, menu_move_down
        scr_offset = (menu_sel_start - menu_top_line_pos) * 
            MENU_ITEM_DISTANCE * SCREEN_WIDTH_BYTES +  MENU_SCR_POS_START;
        draw_menu_item(state, menu_sel_start, scr_offset);
        
        scr_offset = (new_sel - menu_top_line_pos) * 
            MENU_ITEM_DISTANCE * SCREEN_WIDTH_BYTES +  MENU_SCR_POS_START;
        draw_menu_item(state, new_sel, scr_offset);
    } else
    {
        // Hack to make top group text visible again
        state->menu_top_line_pos = (new_sel > FIRST_SELECTABLE_MENU_ITEM) ?
            new_sel : 0;
        draw_white_filled_rect(state->video_buf, MENU_SCR_POS_START,
            MENU_CLEAR_WIDTH, MENU_CLEAR_HEIGHT);
        draw_full_menu(state);
    }
    
    update_screen(state);
}

void menu_move_down(game_state * state, int num_steps)
{
    // Returns the selected menu item after the move
    int menu_i;
    int menu_sel_start = state->menu_selected_item;
    int new_sel = menu_sel_start;
    int count = 0;
    int menu_top_line_pos = state->menu_top_line_pos;
    ushort scr_offset;
    
    if (num_steps <= 0)
        return;
    
    for (menu_i = menu_sel_start + 1; menu_i < NUM_MAIN_MENU_ITEMS; ++menu_i)
    {
        if (game_menu_levels[menu_i].high_scores_index != MENU_GROUP)
        {
            new_sel = menu_i;
            
            if (++count == num_steps)
                break;
        }
    }
    
    if (new_sel == menu_sel_start)
        return;
    
    state->menu_selected_item = new_sel;
    
    if (new_sel < (menu_top_line_pos + MENU_NUM_ITEMS_ON_SCREEN))
    {
        // FIXME? code duplication, menu_move_up
        scr_offset = (menu_sel_start - menu_top_line_pos) * 
            MENU_ITEM_DISTANCE * SCREEN_WIDTH_BYTES +  MENU_SCR_POS_START;
        draw_menu_item(state, menu_sel_start, scr_offset);
        
        scr_offset = (new_sel - menu_top_line_pos) * 
            MENU_ITEM_DISTANCE * SCREEN_WIDTH_BYTES +  MENU_SCR_POS_START;
        draw_menu_item(state, new_sel, scr_offset);
    } else
    {
        state->menu_top_line_pos = new_sel - MENU_NUM_ITEMS_ON_SCREEN + 1;
        draw_white_filled_rect(state->video_buf, MENU_SCR_POS_START,
            MENU_CLEAR_WIDTH, MENU_CLEAR_HEIGHT);
        draw_full_menu(state);
    }
    
    update_screen(state);
}

// Note: uses segment 0 instead of 0x40 to access BIOS memory area
#define BIOS_SEG 0x0
#define BIOS_KBD_FLAGS_1_OFFS 0x417
#define KBD_FLAGS_1_SHIFT 0x3
#define KBD_FLAGS_1_SHIFT_CTRL 0x7

void draw_menu_screen(game_state * state)
{
    draw_empty_game_screen(state->video_buf, MENU_MIDDLE_AREA_BYTES);
    game_screen_help_text(state);
    draw_full_menu(state);
}

void show_high_scores_for_level(game_state * state)
{
    draw_high_sco_window(state, NULL, NULL, state->menu_selected_item);
    update_screen(state);
    
    while (wait_keypress_get_scancode() != SCAN_CODE_ESC) ;
    
    draw_menu_screen(state);
    update_screen(state);
}

#define ERROR_MIDDLE_AREA_BYTES 58
#define ERROR_TEXT_COLOR 0x300
#define ERROR_TEXT_POS_X 4
#define ERROR_TEXT_POS_Y_1 20
#define ERROR_TEXT_POS_Y_2 30

    
void show_error_message(game_state * state, const char _near * message)
{
    draw_empty_game_screen(state->video_buf, ERROR_MIDDLE_AREA_BYTES);
    
    draw_text_6x8(state->video_buf, message, 
        ERROR_TEXT_POS_Y_1 * SCREEN_WIDTH_BYTES + ERROR_TEXT_POS_X,
        DRAW_TEXT_MAX_LEN, ERROR_TEXT_COLOR);
    
    draw_text_6x8(state->video_buf, str_err_window_press_enter, 
        ERROR_TEXT_POS_Y_2 * SCREEN_WIDTH_BYTES + ERROR_TEXT_POS_X,
        DRAW_TEXT_MAX_LEN, ERROR_TEXT_COLOR);
    
    update_screen(state);
   
    while (wait_keypress_get_scancode() != SCAN_CODE_ENTER) ;
}

void play_klo_game(game_state * state, int level_num)
{
    // returns exit code, not STATUS_SUCCESS / _FAILURE
    uchar scancode, kbd_flags_1;
    int high_sco_shown = BOOL_FALSE;
    
    if (new_game(state, level_num))
    {
        draw_game_screen(state);
        update_screen(state);
    } else
    {
        show_error_message(state, str_err_new_game_failure);
        return;
    }

    for (;;)
    {
        scancode = wait_keypress_get_scancode();
        kbd_flags_1 = *(uchar _far *)MK_FP(BIOS_SEG, BIOS_KBD_FLAGS_1_OFFS);
        
        switch (scancode)
        {
            case SCAN_CODE_ESC:
            {
                // Do not show confirm dialog if high_sco_shown is set
                if (high_sco_shown || confirm_dialog(
                        state, str_confirm_exit_main_menu, NULL))
                {
                    return;
                } else
                {
                    draw_game_screen(state);
                    update_screen(state);
                }
                break;
            }
                
            case SCAN_CODE_LEFT:
            {
                if (kbd_flags_1 & KBD_FLAGS_1_SHIFT)
                    move_block_update_screen(state, DIRECTION_LEFT);
                else
                    move_cursor_update_screen(state, DIRECTION_LEFT);
                
                break;
            }
                
            case SCAN_CODE_UP:
            {
                if (kbd_flags_1 & KBD_FLAGS_1_SHIFT_CTRL)
                    move_block_update_screen(state, DIRECTION_UP);
                else
                    move_cursor_update_screen(state, DIRECTION_UP);
                
                break;
            }
                
            case SCAN_CODE_RIGHT:
            {
                if (kbd_flags_1 & KBD_FLAGS_1_SHIFT)
                    move_block_update_screen(state, DIRECTION_RIGHT);
                else
                    move_cursor_update_screen(state, DIRECTION_RIGHT);
                
                break;
            }
                
            case SCAN_CODE_DOWN:
            {
                if (kbd_flags_1 & KBD_FLAGS_1_SHIFT_CTRL)
                    move_block_update_screen(state, DIRECTION_DOWN);
                else
                    move_cursor_update_screen(state, DIRECTION_DOWN);
                break;
            }
                
            case SCAN_CODE_CTRL_LEFT:
            case SCAN_CODE_A:
                move_block_update_screen(state, DIRECTION_LEFT);
                break;
                
            case SCAN_CODE_W:
                move_block_update_screen(state, DIRECTION_UP);
                break;
                
            case SCAN_CODE_CTRL_RIGHT:
            case SCAN_CODE_D:
                move_block_update_screen(state, DIRECTION_RIGHT);
                break;
                
            case SCAN_CODE_S:
                move_block_update_screen(state, DIRECTION_DOWN);
                break;
                
            case SCAN_CODE_R:
            {
                
                if (confirm_dialog(state, str_confirm_restart, NULL))
                {
                    high_sco_shown = BOOL_FALSE;
                    
                    if (!new_game(state, level_num))
                    {
                        show_error_message(state, str_err_new_game_failure);
                        // TODO, state->board was deallocated, cannot continue
                        // However, this is extremely unlikely
                        return;
                    }
                }
                
                draw_game_screen(state);
                update_screen(state);

                break;
            }
            
            case SCAN_CODE_F1:
                show_help(state, HELP_PAGE_CONTROLS_GAME);
                draw_game_screen(state);
                update_screen(state);
                break;
        }
        
        if (state->game_won && !high_sco_shown)
        {
            if (show_high_sco_name_entry_window(state))
            {
                if (!save_high_scores(state))
                    show_error_message(state, str_err_high_sco_save);
            }
            
            draw_game_screen(state);
            update_screen(state);
            high_sco_shown = BOOL_TRUE;
        }
    }
}

#define PG_UP_DN_MOVE 8

void game_menu_loop(game_state * state, int level_num_cmd_line)
{
    int menu_sel;
    
    if (level_num_cmd_line >= 0)
        play_klo_game(state, level_num_cmd_line);
    
    draw_menu_screen(state);
    update_screen(state);
    
    for (;;)
    {
        switch (wait_keypress_get_scancode())
        {
            case SCAN_CODE_ENTER:
            {
                menu_sel = state->menu_selected_item;
                
                switch (game_menu_levels[menu_sel].high_scores_index)
                {
                    case MENU_ABOUT:
                        show_help(state, HELP_PAGE_ABOUT);
                        draw_menu_screen(state);
                        update_screen(state);
                        break;
                        
                    case MENU_KEY_HELP:
                        show_help(state, HELP_PAGE_CONTROLS_MENU);
                        draw_menu_screen(state);
                        update_screen(state);
                        break;
                        
                    case MENU_EXIT:
                        // TODO, temporary, confirm dialog
                        // re-draw entire menu screen if NO was chosen
                        return;
                        
                    default:
                    {
                        play_klo_game(state, menu_sel);
                        draw_menu_screen(state);
                        update_screen(state);
                        break;
                    }
                }
                
                break;
            }
            
            case SCAN_CODE_UP:
                menu_move_up(state, 1);
                break;
                
            case SCAN_CODE_DOWN:
                menu_move_down(state, 1);
                break;
                
            case SCAN_CODE_PGUP:
                menu_move_up(state, PG_UP_DN_MOVE);
                break;
                
            case SCAN_CODE_PGDN:
                menu_move_down(state, PG_UP_DN_MOVE);
                break;
            
            case SCAN_CODE_F1:
                show_help(state, HELP_PAGE_CONTROLS_MENU);
                draw_menu_screen(state);
                update_screen(state);
                break;
                
            case SCAN_CODE_F2:
                show_help(state, HELP_PAGE_ABOUT);
                draw_menu_screen(state);
                update_screen(state);
                break;
                
            case SCAN_CODE_F3:
            case SCAN_CODE_H:    
                show_high_scores_for_level(state);
                break;
            
            case SCAN_CODE_F10:
            /* case SCAN_CODE_ESC: */
                // TODO: temporary, confirm dialog (?)
                // F10 should be quit without confirmation (?)
                return;
        }
    }
}

int main(int argc, const char * argv[])
{
    game_state * state;
    char _far * video_buf; 
    int level_num_cmd_line = -1;
    long level_num_temp;
    char * strtol_end;
    
    _nheapgrow();
    
    video_buf = _fmalloc(CGA_SCREEN_BUF_SIZE);
    
    if (!video_buf)
    {
        puts(str_err_mem_alloc);
        return EXIT_FAILURE;
    }
    
    state = (game_state *)malloc(sizeof(game_state));
    
    if (!state)
    {
        puts(str_err_mem_alloc);
        return EXIT_FAILURE;
    }
    
    state->video_buf = video_buf;
    
    state->high_scores = (high_sco_entry *)malloc(HIGH_SCO_DATA_SIZE);
    
    if (!state->high_scores)
    {
        puts(str_err_mem_alloc);
        return EXIT_FAILURE;
    }
    
    state->blocks = (block_info *)malloc(MAX_NUM_SHAPES * sizeof(block_info));
    
    if (!state->blocks)
    {
        puts(str_err_mem_alloc);
        return EXIT_FAILURE;
    }
    
    state->board = NULL; state->board_temp = NULL;
    state->menu_selected_item = FIRST_SELECTABLE_MENU_ITEM;
    state->menu_top_line_pos = 0;
    
    if (argc == 2)
    {
        level_num_temp = strtol(argv[1], &strtol_end, 10);
        
        if ((*strtol_end == 0) && (level_num_temp >= 0) && 
                (level_num_temp < NUM_MAIN_MENU_ITEMS))
        {
            level_num_cmd_line = (int)level_num_temp;
            
            if (game_menu_levels[level_num_cmd_line].high_scores_index 
                    >= MENU_SPECIAL_FIRST)
                level_num_cmd_line = -1;
        }
    }
    
    set_video_mode(GRAPH_MODE_CGA_320_200);
    
    if (!load_high_scores(state))
    {
        memset(state->high_scores, 0, HIGH_SCO_DATA_SIZE);
        show_error_message(state, str_err_high_sco_load);
    }
    
    game_menu_loop(state, level_num_cmd_line);
    set_video_mode(TEXT_MODE_COLOR_80);
    
    free_game_state_board_ptrs(state);
    free(state->blocks);
    free(state->high_scores);
    free(state);
    _ffree(video_buf);
    
    puts(str_thanks);
    
    return EXIT_SUCCESS;
}

