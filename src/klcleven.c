// KLOCGA program, by knt47

// KLCLEVEN.C: Contains the levels and the items of the game menu
//   With English language text

// The levels are from GNOME Klotski, released under the following license:

/*
   This file is based on: klotski-window.vala from of GNOME Klotski,
   modified for KLOCGA project by knt47

   Copyright (C) 2022 knt47
   Based on: Gnome Klotski
   Copyright (C) 2010-2013 Robert Ancell

   GNOME Klotski is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   GNOME Klotski is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with GNOME Klotski.  If not, see <https://www.gnu.org/licenses/>.
*/

// ( https://gitlab.gnome.org/GNOME/gnome-klotski )

// The levels are located in the klotski-window.vala file:
// https://gitlab.gnome.org/GNOME/gnome-klotski/-/blob/master/src/klotski-window.vala

// Contains 24 from 28 levels from GNOME Klotski due to screen size limits
// TODO: Limitation: Two additional levels would be playable if the
//   tileset was 16x16 instead of 20x20 (Transeuropa, American Pie) (?)

// Currently, the maximum possible width is 11 and maximum height is 10

// Interestingly, the following site:
//   https://sites.google.com/site/klajok/klotski/skill-pack/baltic-sea
//   lists the same five levels as "Skill Pack" as this file and is 
//   missing the additional 4 layouts not available here.

#include "klchead.h"

// The length of level names is limited by the size of the level name box
//   of the game screen. The text can be shown in one or two roes. If two 
//   lines are needed, the name_break field of level_data must be set to a
//   non-zero value, it must be the index of a space character. One line
//   cannot be longer than 9 characters.

const level_data game_menu_levels[NUM_MAIN_MENU_ITEMS] = {
    { 0, 0, MENU_GROUP, 0, "New game - Huarong Trail", NEAR_PTR_NULL },
    { 6, 9, 0, 7, "Only 18 Steps",
        "######"
        "#a**b#"
        "#m**n#"
        "#cdef#"
        "#ghij#"
        "#k  l#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 1, 0, "Daisy",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdef#"
        "#zghi#"
        "#j  k#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 2, 0, "Violet",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdef#"
        "#cghi#"
        "#j  k#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 3, 0, "Poppy",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdde#"
        "#fghi#"
        "#j  k#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 4, 0, "Pansy",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdef#"
        "#cghf#"
        "#i  j#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 5, 0, "Snowdrop",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdde#"
        "#cfgh#"
        "#i  j#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 6, 3, "Red Donkey",
        "######"
        "#a**b#"
        "#a**b#"
        "#cdde#"
        "#cfge#"
        "#h  i#"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 7, 0, "Trail",
        "######"
        "#a**c#"
        "#a**c#"
        "#eddg#"
        "#hffj#"
        "# ii #"
        "##--##"
        "    .."
        "    .." },
    { 6, 9, 8, 0, "Ambush",
        "######"
        "#a**c#"
        "#d**e#"
        "#dffe#"
        "#ghhi#"
        "# jj #"
        "##--##"
        "    .."
        "    .." },
    { 0, 0, MENU_GROUP, 0, "New game - Challenge Pack", NEAR_PTR_NULL },
    { 7, 7, 9, 0, "Agatka", 
        "..     "
        ".      "
        "#####--"
        "#**aab-"
        "#*ccde#"
        "#fgh  #"
        "#######" },
    { 9, 6, 10, 0, "Success",
        "#######  "
        "#**bbc#  "
        "#defgh#  "
        "#ijkgh-  "
        "#llk  #  "
        "#######.." },
    { 6, 9, 11, 0, "Bone",
        "######"
        "#abc*#"
        "# dd*#"
        "# ee*#"
        "# fgh#"
        "##-###"
        "     ."
        "     ."
        "     ." },
    { 7, 10, 12, 0, "Fortune",
        "     .."
        "     . "
        "####-. "
        "#ab  - "
        "#ccd # "
        "#ccd # "
        "#**ee# "
        "#*fgh# "
        "#*iih# "
        "###### " },
    { 10, 6, 13, 0, "Fool", 
        "  ########"
        "  -aabc  #"
        "  #aabdef#"
        "  #ijggef#"
        "  #klhh**#"
        "..########" },
    { 7, 9, 14, 0, "Solomon",
        " .     "
        "..     "
        "#--####"
        "#  aab#"
        "# cdfb#"
        "#hcefg#"
        "#hijk*#"
        "#hll**#"
        "#######" },
    { 6, 8, 15, 0, "Cleopatra",  
        "######"
        "#abcd#"
        "#**ee#"
        "#f*g #"
        "#fh i-"
        "####--"
        "    .."
        "     ." },
    { 11, 8, 16, 0, "Shark",
        "########   "
        "#nrr s #   "
        "#n*op q#   "
        "#***jml#   "
        "#hhijkl#   "
        "#ffcddg-   "
        "#abcdde- . "
        "########..." },
    { 8, 8, 17, 0, "Rome",
        "########"
        "#abcc**#"
        "#ddeef*#"
        "#ddghfi#"
        "#   jki#"
        "#--#####"
        " ..     "
        "  .     "  },
    { 6, 9, 18, 7, "Pennant Puzzle",
        "######"
        "#**aa#"
        "#**bb#"
        "#de  #"
        "#fghh#"
        "#fgii#"
        "#--###"
        "    .."
        "    .." },
    { 0, 0, MENU_GROUP, 0, "New game - Skill Pack", NEAR_PTR_NULL },
    { 9, 8, 19, 0, "Pelopones",
        "#########"
        "#abbb***#"
        "#abbb*c*#"
        "#adeefgg#"
        "#  eefhh#"
        "#... ihh#"
        "#. . ihh#"
        "#########" },
    { 9, 7, 20, 0, "Lodzianka",
        "#########"
        "#**abbcc#"
        "#**abbdd#"
        "#eefgh  #"
        "#iiijk..#"
        "#iiijk..#"
        "#########" },
    { 7, 7, 21, 0, "Polonaise",
        "#######"
        "#aab**#"
        "#aabc*#"
        "#defgg#"
        "#..fhh#"
        "# .ihh#"
        "#######" },
    { 6, 8, 22, 6, "Baltic Sea",
        "######"
        "#.abc#"
        "#.dec#"
        "#fggc#"
        "#fhhi#"
        "#fjk*#"
        "#flk*#"
        "######" },
    { 10, 7, 23, 7, "Traffic Jam",
        "########  "
        "#** ffi#  "
        "#** fgh#  "
        "#aacehh#  "
        "#bbdjlm-  "
        "#bddklm-.."
        "########.." },
    { 0, 0, MENU_GROUP, 0, "Program menu", NEAR_PTR_NULL },
    { 0, 0, MENU_ABOUT, 0, "About KLOCGA", NEAR_PTR_NULL },
    { 0, 0, MENU_KEY_HELP, 0, "Help", NEAR_PTR_NULL },
    { 0, 0, MENU_EXIT, 0, "Exit to DOS", NEAR_PTR_NULL },
};
        

