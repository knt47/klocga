// KLOCGA program, by knt47

// KLCTXTEN.C: English-language strings

/*
  Copyright (c) 2022 knt47

  Redistribution and use in source and binary forms, with or without 
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include "klchead.h"

// Help pages

// Size limit for each line: 35 chars without "\n"
// One help screen can have at most 12 rows.

extern const char * help_pages[NUM_HELP_PAGES] = {
    // HELP_PAGE_CONTROLS_MENU
    "Controls - Main Menu\n\n"
    "\x82\x83\tMove up/down\n"
    "PgUp/PgDn\tMove, 8 steps\n"
    "Enter\tStart level\n"
    "F1\tHelp, menu\n"
    "F2\tAbout KLOCGA\n"
    "F3/H\tHigh scores for level\n"
    "F10\tExit to DOS",
    
    // HELP_PAGE_CONTROLS_GAME
    "Controls - Game\n\n"
    "\x80\x81\x82\x83\tMove cursor (+ shape)\n"
    "WASD /\n"
    "Shift+\x80\x81\x82\x83\tMove block\n"
    "R\tRestart same level\n"
    "F1\tHelp\n"
    "Esc\tExit to main menu\n\n"
    "If you solve a puzzle succesfully,\n"
    "the'high scores'window will appear.\n"
    "If the number of moves you needed\n"
    "to solve the puzzle is in the top 5\n"
    "enter your name, then press ENTER.",
    
    // HELP_PAGE_GAME
    "KLOCGA is a clone of game 'Klotski'\n"
    "for IBM PC compatibles, with CGA\n"
    "graphics. 'Klotski' is a block\n"
    "puzzle game, the goal is to move\n"
    "the block marked with white squares\n"
    "to a target area marked with\n"
    "magenta rectangles. Many levels\n"
    "also contain a magenta 'gate' that\n"
    "can be crossed only by this block.\n"
    "Blocks can be moved in one of four\n"
    "directions (\x80\x81\x82\x83) and only to free\n"
    "squares not occupied by others.",
    
    // HELP_PAGE_ABOUT
    "KLOCGA version 0.1, Copyright (c)\n"
    "  2022 knt47.\n"
    "The level layouts are from GNOME\n"
    "  Klotski, by Robert Ancell,\n"
    "  released under GNU GPL, v3\n"
    "The font is a modified version of\n"
    "  a 6x8 pixel font by Alex Zorg,\n"
    "  available under the 3-clause\n"
    "  BSD license.\n"
    "The rest of the code has been\n"
    "  released under the 2-clause BSD\n"
    "  license by knt47."
};

// Note: size limits are due to the sizes of the boxes on the
//   screen the text need to fit in

// Game screen strings

// Moves: size limit: 6 characters (!)

const char * str_game_scr_moves = "Moves:";

// Level: size limit: 9 characters (including colon)

const char * str_game_scr_level = "Level:";

// Help key: 9 characters

const char * str_game_help_key_1 = "Press F1";
const char * str_game_help_key_2 = "for help";


// Confirm dialog texts, size limit: 21 chars

const char * str_confirm_restart = "Restart same level?";
const char * str_confirm_exit_main_menu = "Exit to main menu?";

// Confirm dialog "buttons", size limit: ??

const char * str_confirm_yes = "Yes";
const char * str_confirm_no = "No";

// Help window strings, size limit is 35 unless not a full line 
//   (str_text_help_page)

const char * str_text_help_page = "KLOCGA help, page";
const char * str_text_help_keys = "\x80\x81 - Select page, ESC - close";

// High scores window strings, size limit: ??

const char * str_high_sco_level_row = "High scores for";
const char * str_high_sco_bottom_row_default = "Press ESC to close.";
const char * str_high_sco_top_row_level_won = 
    "Congratulations! Level completed.";
const char * str_high_sco_bottom_row_name_enter =
    "Enter your name and press ENTER.";

// In-game error messages, size limit: 38 chars

const char * str_err_new_game_failure = 
    "ERROR, failed to start selected level.";
const char * str_err_high_sco_load = "ERROR, Could not load high scores.";
const char * str_err_high_sco_save = "ERROR, Could not save high scores.";
const char * str_err_window_press_enter = "Press ENTER to continue.";

// Error messages printed into DOS console if game initialization fails

const char * str_err_mem_alloc = "ERROR, memory allocation failure.\n";

// "Thanks for playing" message

const char * str_thanks = "Thanks for playing.";

