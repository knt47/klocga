// KLOCGA program, by knt47

// KLCGRAPH.C: CGA graphics routines

/*
  Copyright (c) 2022 knt47

  Redistribution and use in source and binary forms, with or without 
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include "klchead.h"

// Comment out the following line for usinf the "old" copy to screen code
#define USE_NEW_COPY_TO_SCREEN_CODE
// If the next line is commented out, the program will never wait for
//   vertical blanking when the update_screen routine is called
// #define COPY_TO_SCREEN_WAT_FOR_VBL

#ifdef USE_NEW_COPY_TO_SCREEN_CODE

extern void cga_copy_to_screen(char _far * video_buf);

#ifdef COPY_TO_SCREEN_WAT_FOR_VBL

// Copy the contents of the video (double) buffer into CGA video memory.
// NOTE 1: CGA uses a weird memory alignment for framebuffer: It is divided 
//   into two halves, the even-numbered rows start at offset 0ya and the odd-
//   numbered ones at 0x2000. In contrast, video_buf is fully "linear".
// NOTE 2: Watcom seems to disallow usage of ds for far pointer argument 
//   in medium memory model in pragma aux. Therefore a somewhat weird
//   but allowed register combination was chosen from the manual.
// NOTE 3: the value of bh (at the start of the _l1 loop) must be even.

#pragma aux cga_copy_to_screen = \
    "push    ds" \
    "mov     ds, dx" \
    "mov     dx, 0b800h" \
    "mov     es, dx" \
    "xor     di, di" \
    "xor     ch, ch" \
    "mov     dx, 3dah" \
    "mov     bl, 0ah" \
    "cga_copy_to_screen_l1:" \
    "mov     bh, 14h" \
    "cga_copy_to_screen_l2:" \
    "in      al, dx" \
    "test    al, 8h" \
    "jnz     short cga_copy_to_screen_l2" \
    "cga_copy_to_screen_l3:" \
    "in      al, dx" \
    "test    al, 8h" \
    "jz      short cga_copy_to_screen_l3" \
    "lea     ax, [di + 2000h]"\
    "cga_copy_to_screen_l4:" \
    "mov     cl, 28h" \
    "rep     movsw" \
    "xchg    ax, di" \
    "dec     bh" \
    "jnz     cga_copy_to_screen_l4" \
    "dec     bl" \
    "jnz     cga_copy_to_screen_l1" \
    "pop     ds" \
    __parm [dx si] \
    __modify [ax bx cx dx si di es];
    
#else
    
#pragma aux cga_copy_to_screen = \
    "push    ds" \
    "mov     ds, dx" \
    "mov     dx, 0b800h" \
    "mov     es, dx" \
    "xor     di, di" \
    "xor     ch, ch" \
    "mov     ax, 2000h" \
    "mov     dx, 0c8h" \
    "cga_copy_to_screen_l1:" \
    "mov     cl, 28h" \
    "rep     movsw" \
    "xchg    ax, di" \
    "dec     dx" \
    "jnz     cga_copy_to_screen_l1" \
    "pop     ds" \
    __parm [dx si] \
    __modify [ax cx dx si di es];

#endif

void update_screen(game_state * state)
{
    cga_copy_to_screen(state->video_buf);
}

    
#else

// Wait for vertical blanking

#ifdef COPY_TO_SCREEN_WAT_FOR_VBL

extern void wait_for_vert_blanking(void);

#pragma aux wait_for_vert_blanking = \
    "mov     dx, 3dah" \
    "wait_vert_bl_l1:" \
    "in      al, dx" \
    "test    al, 8h" \
    "jnz     short wait_vert_bl_l1" \
    "wait_vert_bl_l2:" \
    "in      al, dx" \
    "test    al, 8h" \
    "jz      short wait_vert_bl_l2" \
    __modify [ax dx];
    
#endif
    
// See notes 1 and 2 for cga_copy_to_screen above
    
extern void cga_copy_to_screen(char _far * video_buf);

#pragma aux cga_copy_to_screen = \
    "push    ds" \
    "mov     ds, dx" \
    "mov     dx, 0b800h" \
    "mov     es, dx" \
    "xor     di, di" \
    "xor     ch, ch" \
    "mov     si, bx" \
    "mov     dx, 64h" \
    "cga_copy_to_screen_l1:" \
    "mov     cl, 28h" \
    "rep     movsw" \
    "add     si, 50h" \
    "dec     dx" \
    "jnz     cga_copy_to_screen_l1" \
    "mov     di, 2000h" \
    "lea     si, [bx + 50h]" \
    "mov     dl, 64h" \
    "cga_copy_to_screen_l2:" \
    "mov     cl, 28h" \
    "rep     movsw" \
    "add     si, 50h" \
    "dec     dx" \
    "jnz     cga_copy_to_screen_l2" \
    "pop     ds" \
    __parm [dx bx] \
    __modify [cx dx si di es];


void update_screen(game_state * state)
{
#ifdef COPY_TO_SCREEN_WAT_FOR_VBL
    wait_for_vert_blanking();
#endif
    cga_copy_to_screen(state->video_buf);
}

#endif

// Draw text to the video buffer using the 6x8 font.
// video_buf: es:di = video buffer
// scr_offset: ax = relative offset in video buffer
// font_data: bx = offset of character array (must be in ds)
// num_chars_max: cl = number of characters to print (only values 0x0..0x7f  
//   are valid, this is not a problem because only 53 chars can fit in a row 
//   and line breaks are not supported.)
// colors: dl = foreground color (0..3)
//   dh = bits 0..1 -> background color (0..3)
//   bit 7: start drawing at a half byte (i.e. x % 4 == 2)
// Destroys the values of all registers except bx, bp, sp, segments.
// Asm NOTE 1: Printing stops either at a NUL ('\0') terminator or
//   after num_chars characters have been printed. Can print at most 0x7f
//   characters (DRAW_TEXT_MAX_LEN), see comment above.
// Asm NOTE 2: AH is only minimally used, (TODO ...)
// Asm NOTE 3: Special treatment for the last byte if it starts at 
// half-byte (TODO, add description)
// Asm NOTE 4: ror ch, 1: CF will be set if the value of ch after ror is 80h -
//   when the original value was "restored" after 8 rotation. This can be 
//   exploited for "loop control".
// Asm NOTE 5: adc di, -27fh:  Simple trick for subtracting 8h * 50h - 1h 
//   from di if the most significant bit of ch is 1, 8h * 50h - 2h otherwise
// Asm NOTE 6: Written out "negative jumps" due to jump size limit

// HACK, problems with accessing chars_font_6x8_0x20_0x81 form inline asm
//   passing its offset in bx instead

extern void draw_text_6x8_asm(char _far * video_buf,
    const uchar _near * font_data, const char * text, ushort scr_offset,
    ushort num_chars_max, ushort colors);
    
#pragma aux draw_text_6x8_asm = \
    "push    bp" \
    "mov     bp, bx" \
    "add     di, ax" \
    "and     cl, 7fh" \
    "jz      draw_6x8_text_l3" \
    "xor     dh, dl" \
    "mov     ch, 80h" \
    "or      dh, dh" \
    "jns     draw_6x8_text_l2" \
    "or      cl, ch" \
    "and     dh, 7fh" \
    "lea     bx, [di + 280h]" \
    "draw_6x8_text_l1:" \
    "sub     bx, 50h" \
    "and     byte ptr [es:bx], 0f0h" \
    "cmp     bx, di" \
    "ja      draw_6x8_text_l1" \
    "draw_6x8_text_l2:" \
    "xor     ah, ah" \
    "lodsb" \
    "or      al, al" \
    "jnz     draw_6x8_text_l4" \
    "draw_6x8_text_l3:" \
    "jmp     draw_6x8_text_l20" \
    "draw_6x8_text_l4:" \
    "sub     al, 20h" \
    "mov     bx, ax" \
    "shl     bx, 1" \
    "add     bx, ax" \
    "shl     bx, 1" \
    "add     bx, bp" \
    "xor     cl, 80h" \
    "draw_6x8_text_l5:" \
    "xor     al, al" \
    "or      cl, cl" \
    "js      draw_6x8_text_l8  " \
    "or      al, dl" \
    "test    [bx], ch" \
    "jnz     draw_6x8_text_l6" \
    "xor     al, dh" \
    "draw_6x8_text_l6:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "or      al, dl" \
    "test    [bx + 1], ch" \
    "jnz     draw_6x8_text_l7" \
    "xor     al, dh" \
    "draw_6x8_text_l7:" \
    "or      [es:di], al" \
    "inc     di" \
    "inc     bx" \
    "inc     bx" \
    "xor     al, al" \
    "draw_6x8_text_l8:" \
    "or      al, dl" \
    "test    [bx], ch" \
    "jnz     draw_6x8_text_l9" \
    "xor     al, dh" \
    "draw_6x8_text_l9:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "or      al, dl" \
    "test    [bx + 1h], ch" \
    "jnz     draw_6x8_text_l10" \
    "xor     al, dh" \
    "draw_6x8_text_l10:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "or      al, dl" \
    "test    [bx + 2h], ch" \
    "jnz     draw_6x8_text_l11" \
    "xor     al, dh" \
    "draw_6x8_text_l11:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "or      al, dl" \
    "test    [bx + 3h], ch" \
    "jnz     draw_6x8_text_l12" \
    "xor     al, dh" \
    "draw_6x8_text_l12:" \
    "mov     [es:di], al" \
    "or      cl, cl" \
    "jns     draw_6x8_text_l17" \
    "xor     al, al" \
    "or      al, dl" \
    "test    [bx + 4h], ch" \
    "jnz     draw_6x8_text_l13" \
    "xor     al, dh" \
    "draw_6x8_text_l13:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "or      al, dl" \
    "test    [bx + 5h], ch" \
    "jnz     draw_6x8_text_l14" \
    "xor     al, dh" \
    "draw_6x8_text_l14:" \
    "shl     al, 1" \
    "shl     al, 1" \
    "shl     al, 1" \
    "shl     al, 1" \
    "cmp     [si], 0h" \
    "je      draw_6x8_text_l15" \
    "cmp     cl, 81h" \
    "jne     draw_6x8_text_l16" \
    "draw_6x8_text_l15:" \
    "mov     ah, 0fh" \
    "and     ah, [es:di + 1h]" \
    "or      al, ah" \
    "draw_6x8_text_l16:" \
    "mov     [es:di + 1h], al" \
    "jmp     draw_6x8_text_l18" \
    "draw_6x8_text_l17:" \
    "dec     bx" \
    "dec     bx" \
    "dec     di " \
    "draw_6x8_text_l18:" \
    "add     di, 50h" \
    "ror     ch, 1" \
    "jc      draw_6x8_text_l19" \
    "jmp     draw_6x8_text_l5" \
    "draw_6x8_text_l19:" \
    "cmp     cl, 80h" \
    "adc     di, -27fh" \
    "dec     cl" \
    "test    cl, 7fh" \
    "jz      draw_6x8_text_l20" \
    "jmp     draw_6x8_text_l2" \
    "draw_6x8_text_l20:" \
    "pop     bp" \
    __parm [es di] [bx] [si] [ax] [cx] [dx] \
    __modify [ax bx cx dx si di];
    
void draw_text_6x8(char _far * video_buf, const char * text,
    ushort scr_offset, ushort num_chars_max, ushort colors)
{
    // Simple "container" func for inline assembly code.
    draw_text_6x8_asm(video_buf, chars_font_6x8_0x20_0x84, text,
        scr_offset, num_chars_max, colors);
}
    
// get_tileset_indices: Fills an array of 4 bytes with the indices of quarter
//   tiles in the tileset for a specific square of board. Check all 8 
//   neighbors of the square (TODO, 4 should be enough in most cases.)
// Handles multiple special cases: ' ', '.' (target square), '-' ("gate" for 
//   target block), 80h bit for blocks occupying the '.' squares, 80h
//   corresponding to '*' crossing '-' squares. quarter_tileset_indices does
//   not contain the data required for 270 degree angles (the only case when
//   "diagonal neighbors" are relevant) this is also handled in this function.
// si = ptr to one square in board array (one byte)
// bx = row width ( + extra 2 bytes), di = output 4-byte array
// 20h -> ' ', 2ah -> '*' 2dh -> '-'  2eh -> '.'
// Diagonal neighbor bits in al: Left,Up 0x10  Right,Up 0x20  
//   Left,Bottom  0x40  Right,Bottom  0x80. For horizontal/vertical,
//   see quarter_tileset_indices


// HACK, problems with accessing quarter_tileset_indices form inline asm
//   passing its offset in cx instead
    
extern void get_tileset_indices(uchar _near * out_indices, 
    char _near * board_square, uchar _near * q_tileset_index_arr, 
    ushort row_width_bytes);

#pragma aux get_tileset_indices = \
    "mov     al, [si]" \
    "and     al, 7fh" \
    "jnz     tileset_indices_l1" \
    "mov     al, 2ah" \
    "jmp     tileset_indices_l5" \
    "tileset_indices_l1:" \
    "cmp     al, 20h" \
    "jne     tileset_indices_l2" \
    "xor     dx, dx" \
    "mov     [di], dx" \
    "jmp     tileset_indices_l4" \
    "tileset_indices_l2:" \
    "cmp     al, 2dh" \
    "jne     tileset_indices_l3" \
    "mov     word ptr [di], 0f0eh" \
    "mov     dx, 1110h" \
    "jmp     tileset_indices_l4" \
    "tileset_indices_l3:" \
    "cmp     al, 2eh" \
    "jne     tileset_indices_l5" \
    "mov     word ptr [di], 1312h" \
    "mov     dx, 1514h" \
    "tileset_indices_l4:" \
    "jmp     tileset_indices_l19" \
    "tileset_indices_l5:" \
    "mov     dh, al" \
    "mov     ax, 7f00h" \
    "tileset_indices_l6:" \
    "mov     dl, [si - 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l7" \
    "or      al, 1h" \
    "tileset_indices_l7:" \
    "mov     dl, [si + 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l8" \
    "or      al, 4h" \
    "tileset_indices_l8:" \
    "mov     dl, [bx + si]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l9" \
    "or      al, 8h" \
    "tileset_indices_l9:" \
    "mov     dl, [bx + si - 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l10" \
    "or      al, 40h" \
    "tileset_indices_l10:" \
    "mov     dl, [bx + si + 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l11" \
    "or      al, 80h" \
    "tileset_indices_l11:" \
    "neg     bx" \
    "mov     dl, [bx + si]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l12" \
    "or      al, 2h" \
    "tileset_indices_l12:" \
    "mov     dl, [bx + si - 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l13" \
    "or      al, 10h" \
    "tileset_indices_l13:" \
    "mov     dl, [bx + si + 1h]" \
    "and     dl, ah" \
    "cmp     dl, dh" \
    "jne     tileset_indices_l14" \
    "or      al, 20h" \
    "tileset_indices_l14:" \
    "cmp     dh, 2ah" \
    "jne     tileset_indices_l15" \
    "xor     dh, dh" \
    "neg     bx" \
    "jmp     tileset_indices_l6" \
    "tileset_indices_l15:" \
    "mov     bx, 0fh" \
    "and     bl, al" \
    "shl     bx, 1" \
    "shl     bx, 1" \
    "add     bx, cx" \
    "mov     dx, [bx]" \
    "mov     ah, 1h" \
    "cmp     dl, ah" \
    "jne     tileset_indices_l16" \
    "test    al, 10h" \
    "jnz     tileset_indices_l16" \
    "mov     dl, 0ah" \
    "tileset_indices_l16:" \
    "cmp     dh, ah" \
    "jne     tileset_indices_l17" \
    "test    al, 20h" \
    "jnz     tileset_indices_l17" \
    "mov     dh, 0bh" \
    "tileset_indices_l17:" \
    "mov     [di], dx" \
    "mov     dx, [bx + 2h]" \
    "cmp     dl, ah" \
    "jne     tileset_indices_l18" \
    "test    al, 40h" \
    "jnz     tileset_indices_l18" \
    "mov     dl, 0ch" \
    "tileset_indices_l18:" \
    "cmp     dh, ah" \
    "jne     tileset_indices_l19" \
    "test    al, 80h" \
    "jnz     tileset_indices_l19" \
    "mov     dh, 0dh" \
    "tileset_indices_l19:" \
    "mov     [di + 2h], dx" \
    __parm [di] [si] [cx] [bx] \
    __modify [ax bx dx];


// Subroutines for drawing a "quarter title" into the double buffer
//   (NOT the screen)
// es:di - buffer, cx - rel. screen offset, 
// si - start of quarter tileset array, ax - tile number
// Note: draw_quarter_tile_right_part must be called after
//   drawing the left part, otherwise, it will not work correctly
// Note: ax is multiplied by (12 / 4) * 10 = 30 = 32 - 2

extern void draw_quarter_tile_left_part(char _far * video_buf,
    const uchar _near * quarter_tile_data, ushort scr_offset,
    ushort tile_num);

#pragma aux draw_quarter_tile_left_part = \
    "add     di, cx" \
    "mov     dx, ax" \
    "mov     cl, 5h" \
    "shl     ax, cl" \
    "sub     ax, dx" \
    "sub     ax, dx" \
    "add     si, ax" \
    "mov     cx, 0ah" \
    "draw_q_tile_left_l1:" \
    "movsw" \
    "lodsb" \
    "and     al, 0f0h" \
    "stosb" \
    "add     di, 4dh" \
    "loop    draw_q_tile_left_l1" \
    __parm [es di] [si] [cx] [ax] \
    __modify [ax cx dx si di];
    
extern void draw_quarter_tile_right_part(char _far * video_buf,
    const uchar _near * quarter_tile_data, ushort scr_offset,
    ushort tile_num);

#pragma aux draw_quarter_tile_right_part = \
    "add     di, cx" \
    "mov     dx, ax" \
    "mov     cl, 5h" \
    "shl     ax, cl" \
    "sub     ax, dx" \
    "sub     ax, dx" \
    "add     si, ax" \
    "mov     cx, 0ah" \
    "draw_q_tile_right_l1:" \
    "lodsb" \
    "and     al, 0fh" \
    "or      [es:di], al" \
    "inc     di" \
    "movsw" \
    "add     di, 4dh" \
    "loop    draw_q_tile_right_l1" \
    __parm [es di] [si] [cx] [ax] \
    __modify [ax cx dx si di];
    
// Subroutine for drawing the square signs that distinguish the target
//   block from other blocks. Its color changes from white to pink if
//   the block is moved to the target squares.
    
extern void draw_target_block_squares(char _far * video_buf, 
    ushort scr_offset, char board_square_ch);

#pragma aux draw_target_block_squares = \
    "mov     al, ah" \
    "and     al, 7fh" \
    "jz      target_block_sq_l1" \
    "cmp     al, 2ah" \
    "jne     target_block_sq_l3" \
    "mov     al, 28h" \
    "or      ah, ah" \
    "js      target_block_sq_l2" \
    "target_block_sq_l1:" \
    "mov     al, 3ch" \
    "target_block_sq_l2:" \
    "add     di, cx" \
    "add     di, 2d2h" \
    "mov     byte ptr [es:di - 50h], 0h" \
    "stosb" \
    "add     di, 4fh" \
    "stosb" \
    "add     di, 4fh" \
    "xor     al, al" \
    "stosb" \
    "target_block_sq_l3: " \
    __parm [es di] [cx] [ah] \
    __modify [al di];
    
// Subroutine for drawing the tile selection cursor ('+' shape)
// TODO, smaller code size (?)
    
extern void draw_cursor(char _far * video_buf, ushort scr_offset);

#pragma aux draw_cursor = \
    "add     ax, 2d1h" \
    "add     di, ax" \
    "mov     al, 0c3h" \
    "and     [es:di - 13fh], al" \
    "and     [es:di - 0efh], al" \
    "and     [es:di - 9fh], al" \
    "and     [es:di + 0f1h], al" \
    "and     [es:di + 141h], al" \
    "and     [es:di + 191h], al" \
    "mov     ax, 3c0h" \
    "and     [es:di], al" \
    "and     [es:di + 2h], ah" \
    "and     [es:di + 50h], al" \
    "and     [es:di + 52h], ah" \
    __parm [es di] [ax] \
    __modify [ax di];
    
// The position of the right edge of the board on the screen
//   in bytes, not pixels.
#define BOARD_SCREEN_POS_RIGHT_BYTES 58
// TODO, rename, refers to bard row, not screen row
#define SCREEN_HALF_ROW_BYTES (TILE_HALF_HEIGHT_PX * SCREEN_WIDTH_BYTES)
#define SCREEN_FULL_ROW_BYTES (SCREEN_HALF_ROW_BYTES * 2)
#define INDICES_ARR_SIZE 4
    
void draw_board(game_state * state, int start_x, int start_y,
    int end_x, int end_y)
{
    // Note: Intervals for x and y coordinates do not include end_x, end_y
    // Note: The graphics is centered along y axis but right aligned
    //   along x axis
    char _far * video_buf = state->video_buf;
    int x, y, board_i, screen_offs;
    int screen_start_x_d4, screen_start_y;
    int draw_squares_x, draw_squares_y, scr_next_row_incr, incr_end_row;
    int width = state->width;
    uchar indices_arr[INDICES_ARR_SIZE];
    
    // Note: modifies args num_squares_x, num_squares_y
    
    draw_squares_x = (end_x > width ? width : end_x) - start_x;
    draw_squares_y = (end_y > state->height ? 
        state->height : end_y) - start_y;
    scr_next_row_incr = SCREEN_FULL_ROW_BYTES - 
        draw_squares_x * TILE_WIDTH_BYTES;
    incr_end_row = width - draw_squares_x + 2;
    
    board_i = (start_y + 1) * (width + 2) + start_x + 1;
    
    // Note: If the following code is modified, draw_game_screen (klcgame)
    //   must be also changed
    screen_start_x_d4 = ((width > 9) ? BOARD_SCREEN_POS_RIGHT_BYTES + 2 :
        BOARD_SCREEN_POS_RIGHT_BYTES) + (start_x - width) * TILE_WIDTH_BYTES;
    screen_start_y = (SCREEN_HEIGHT_PIXELS >> 1) + 
        (start_y * 2 - state->height) * TILE_HALF_HEIGHT_PX;
    screen_offs = screen_start_y * SCREEN_WIDTH_BYTES + screen_start_x_d4;
    
    for (y = 0; y < draw_squares_y; ++y)
    {
        for (x = 0; x < draw_squares_x; ++x)
        {
            get_tileset_indices(indices_arr, state->board + board_i,
                (uchar _near *)quarter_tileset_indices, (ushort)(width + 2));
            
            draw_quarter_tile_left_part(video_buf, quarter_tile_data_cga,
                screen_offs, (ushort)indices_arr[0]);
            draw_quarter_tile_right_part(video_buf, quarter_tile_data_cga,
                screen_offs + 2, (ushort)indices_arr[1]);
            draw_quarter_tile_left_part(video_buf, quarter_tile_data_cga,
                screen_offs + SCREEN_HALF_ROW_BYTES, (ushort)indices_arr[2]);
            draw_quarter_tile_right_part(video_buf, quarter_tile_data_cga,
                screen_offs + SCREEN_HALF_ROW_BYTES + 2,
                (ushort)indices_arr[3]);
            
            draw_target_block_squares(video_buf, screen_offs,
                state->board[board_i]);
            
            if (((x + start_x) == state->cur_x) && 
                    ((y + start_y) == state->cur_y))
                draw_cursor(video_buf, screen_offs);
            
            ++board_i; screen_offs += TILE_WIDTH_BYTES;
        }
        
        board_i += incr_end_row; screen_offs += scr_next_row_incr;
    }
}


// Draws the main regions of game screen into the video buffer: Two areas
//    with a "dithered" magenta-black pattern in the left & right, the left
//    area has a variable size (area_left_bytes). The middle is the game/menu
//    area with a white background.

extern void draw_empty_game_scr_asm(char _far * video_buf,
    ushort area_middle_bytes);

#pragma aux draw_empty_game_scr_asm = \
    "mov     dx, 88c8h" \
    "mov     al, dh" \
    "empty_game_screen_l1:" \
    "mov     cx, 3ch" \
    "sub     cx, bx" \
    "rep     stosb" \
    "xor     dh, 0aah" \
    "and     al, 0c0h" \
    "or      al, 0fh" \
    "stosb" \
    "mov     al, 0ffh" \
    "mov     cx, bx" \
    "rep     stosb" \
    "mov     al, dh" \
    "and     al, 3h" \
    "or      al, 0f0h" \
    "stosb" \
    "mov     al, dh" \
    "mov     cl, 12h" \
    "rep     stosb" \
    "dec     dl" \
    "jnz     short empty_game_screen_l1" \
    __parm [es di] [bx] \
    __modify [ax cx dx di];
    
// Draws the logo to the top of the right area of the screen.
//    Width and height are passed as arguments.
    
extern void draw_game_logo(char _far * video_buf,
    const uchar _near * logo_data, ushort scr_offset,
    ushort width_bytes, ushort height_pixels);

#pragma aux draw_game_logo = \
    "add     di, cx" \
    "draw_logo_l1:" \
    "mov     cx, dx" \
    "rep     movsb" \
    "add     di, 50h" \
    "sub     di, dx" \
    "dec     ax" \
    "jnz     draw_logo_l1" \
    __parm [es di] [si] [cx] [dx] [ax] \
    __modify [ax cx si di];
    
#define LOGO_DRAW_POS (SCREEN_WIDTH_BYTES * 6 + 65)
    
void draw_empty_game_screen(char _far * video_buf, ushort area_middle_bytes)
{
    draw_empty_game_scr_asm(video_buf, area_middle_bytes);
    // TODO, add logo drawing code, ... (??)
    draw_game_logo(video_buf, game_logo_cga, LOGO_DRAW_POS,
        LOGO_BYTES_IN_ROW, LOGO_NUM_ROWS);
}
    
// Draws a white rectangle with a black border. The function is extremely,
//   simple and limited. The rectangle must start at an x position divisible
//   by 4 must end at an x position where x % 4 == 3 and must be at least
//   7 pixels wide.
    
extern void draw_white_rect_with_border_asm(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels);
    
#pragma aux draw_white_rect_with_border_asm = \
    "add     di, ax" \
    "mov     cx, bx" \
    "xor     al, al" \
    "rep     stosb" \
    "add     di, 50h" \
    "sub     di, bx" \
    "draw_white_rect_border_l1:" \
    "mov     al, 3fh" \
    "stosb" \
    "lea     cx, [bx - 2h]" \
    "mov     al, 0ffh" \
    "rep     stosb" \
    "mov     al, 0fch" \
    "stosb" \
    "add     di, 50h" \
    "sub     di, bx" \
    "dec     dx" \
    "jnz     draw_white_rect_border_l1" \
    "mov     cx, bx" \
    "xor     al, al" \
    "rep     stosb" \
    __parm [es di] [ax] [bx] [dx] \
    __modify [ax cx dx di];

void draw_white_rect_with_border(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels)
{
    // Simple "container" func for inline assembly code.
    draw_white_rect_with_border_asm(video_buf, scr_offset,
        width_bytes, height_pixels);
}


// Draws a white filled rectangle. The same restrictions apply as in
//   draw_white_rect_with_border_asm. Currently only used for clearing
//   background in the menu drawing code.

extern void draw_white_filled_rect_asm(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels);
    
#pragma aux draw_white_filled_rect_asm = \
    "add     di, ax" \
    "mov     al, 0ffh" \
    "draw_white_filled_rect_l1:" \
    "mov     cx, bx" \
    "rep     stosb" \
    "add     di, 50h" \
    "sub     di, bx" \
    "dec     dx" \
    "jnz     draw_white_filled_rect_l1" \
    __parm [es di] [ax] [bx] [dx] \
    __modify [ax cx dx di];
    
void draw_white_filled_rect(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels)
{
    // Simple "container" func for inline assembly code.
    draw_white_filled_rect_asm(video_buf, scr_offset,
        width_bytes, height_pixels);
}

#define TAB_REL_POS_X_BYTES 18
#define HELP_LINE_DISTANCE 10

void draw_multi_line_text(char _far * video_buf,
    const char * text, ushort scr_offset, ushort colors)
{
    // NOTE: only supports a single TAB per row
    char _near * text_ptr = (char _near *)text;
    char _near * write_start = text_ptr;
    int scr_pos = scr_offset;
    char ch_current;
    char ch_end_last = '\n';
    
    for (;;)
    {
        ch_current = *text_ptr++;
        
        switch (ch_current)
        {
            case '\n':
            case '\t':
            case 0:
                draw_text_6x8(video_buf, write_start, 
                    (ch_end_last == '\t') ? 
                        scr_pos + TAB_REL_POS_X_BYTES : scr_pos,
                    text_ptr - write_start - 1, colors);
                
                if (ch_current == 0)
                    return;
                
                if (ch_current == '\n')
                    scr_pos += HELP_LINE_DISTANCE * SCREEN_WIDTH_BYTES; 
                
                write_start = text_ptr;
                ch_end_last = ch_current;
                break;
        }
        
    }
}
