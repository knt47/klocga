// KLOCGA program, by knt47

/*
  Copyright (c) 2022 knt47

  Redistribution and use in source and binary forms, with or without 
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef KLCHEAD_H
#define KLCHEAD_H

// === Number of main menu entries, blocks, font chars, ... ===

#define NUM_MAIN_MENU_ITEMS 31
#define NUM_LEVELS_HIGH_SCO 24
#define NUM_QUARTER_TILES 22
#define NUM_FONT_CHARS 101
#define NUM_HELP_PAGES 4

// === Types and structs ===

typedef unsigned char uchar;
typedef unsigned short ushort;

#define HIGH_SCO_ENTRY_SIZE 16
#define HIGH_SCO_NAME_SIZE (HIGH_SCO_ENTRY_SIZE - sizeof(ushort))

#pragma pack(push, 1)

typedef struct _block_info
{
    char block_char;
    uchar width, height;
    uchar pos_x, pos_y;
} block_info;

typedef struct _high_sco_entry
{
    char player_name[HIGH_SCO_NAME_SIZE];
    ushort num_moves;
} high_sco_entry;

typedef struct _game_state 
{
    char _near * board;
    char _near * board_temp;
    char _far * video_buf;
    block_info _near * blocks;
    high_sco_entry _near * high_scores;
    int num_blocks;
    int level_num;
    int width, height;
    // Note: size is actually (width + 2) * (height + 2)
    int size;
    int cur_x, cur_y;
    int num_moves;
    int target_block_index;
    int menu_selected_item;
    int menu_top_line_pos;
    uchar target_area_x, target_area_y;
    uchar game_won;
    char prev_moved_block;
} game_state;

typedef struct _level_data
{
    uchar width, height;
    // high_scores_index: Either an index in "High Scores" or 
    //   a menu command ID
    uchar high_scores_index;
    // name_break: if not 0, a line break is used at this position
    //   (the char should be a space)
    uchar name_break;
    const char _near * level_name;
    const char _near * level;
} level_data;

#pragma pack(pop)

// === Misc constants ===

#define NEAR_PTR_NULL ((void _near *)0)

#define STATUS_SUCCESS 1
#define STATUS_FAILURE 0

#define BOOL_FALSE 0
#define BOOL_TRUE 1

#define DIRECTION_LEFT 0
#define DIRECTION_UP 1
#define DIRECTION_RIGHT 2
#define DIRECTION_DOWN 3
#define DIRECTION_BIT_VERTICAL 1

#define MAX_NUM_SHAPES 32

#define CGA_SCREEN_BUF_SIZE 16000

// === Menu command IDs ===

#define MENU_ABOUT 0xfc
#define MENU_KEY_HELP 0xfd
#define MENU_EXIT 0xfe
#define MENU_GROUP 0xff
#define MENU_SPECIAL_FIRST MENU_ABOUT

#define FIRST_SELECTABLE_MENU_ITEM 1

// === Levels ===

extern const level_data game_menu_levels[NUM_MAIN_MENU_ITEMS];

// === 6 x 8 pixel font ===

#define FONT_6X8_CHAR_SIZE 6
#define FONT_6X8_DATA_SIZE (NUM_FONT_CHARS * FONT_6X8_CHAR_SIZE)

extern const uchar chars_font_6x8_0x20_0x84[FONT_6X8_DATA_SIZE];

// === Tile & logo graphics ===

#define QUARTER_TILE_SIZE 30
#define QUARTER_TILE_DATA_SIZE (QUARTER_TILE_SIZE * NUM_QUARTER_TILES)
#define QUARTER_TILESET_INDICES_SIZE 64

#define LOGO_NUM_ROWS 52
// Note: width is actually 48 pixels
#define LOGO_BYTES_IN_ROW 12
#define LOGO_DATA_SIZE (LOGO_NUM_ROWS * LOGO_BYTES_IN_ROW)

extern const uchar quarter_tile_data_cga[QUARTER_TILE_DATA_SIZE];
extern const uchar quarter_tileset_indices[QUARTER_TILESET_INDICES_SIZE];

extern const uchar game_logo_cga[LOGO_DATA_SIZE];

// === Graphics routines & related consts ===

#define TILE_WIDTH_BYTES 5
#define TILE_HALF_HEIGHT_PX 10
#define SCREEN_WIDTH_BYTES 80
#define SCREEN_HEIGHT_PIXELS 200

#define DRAW_TEXT_MAX_LEN 0x7f
#define DRAW_BOARD_FULL 0x7fff

extern void update_screen(game_state * state);
extern void draw_text_6x8(char _far * video_buf, const char * text,
    ushort scr_offset, ushort num_chars, ushort colors);
extern void draw_board(game_state * state, int start_x, int start_y,
    int end_x, int end_y);
extern void draw_empty_game_screen(char _far * video_buf,
    ushort area_middle_bytes);
extern void draw_white_rect_with_border(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels);
extern void draw_white_filled_rect(char _far * video_buf,
    ushort scr_offset, ushort width_bytes, ushort height_pixels);
extern void draw_multi_line_text(char _far * video_buf,
    const char * text, ushort scr_offset, ushort colors);

// === Game graphics, rules, ... ===

// free_game_state_board_ptrs - temporary ??
extern void free_game_state_board_ptrs(game_state * state);
extern int new_game(game_state * state, int level_number);
extern void game_screen_help_text(game_state * state);
extern void draw_game_screen(game_state * state);
extern int move_block_update_screen(game_state * state, int direction);
extern int move_cursor_update_screen(game_state * state, int direction);
extern int confirm_dialog(game_state * state, const char * row_1_text,
    const char * row_2_text);
extern void show_help(game_state * state, ushort page_num_start);

// === High scores ===

#define HIGH_SCO_ENTRIES_PER_LEVEL 6
#define HIGH_SCO_DATA_SIZE (HIGH_SCO_ENTRIES_PER_LEVEL * \
    NUM_LEVELS_HIGH_SCO * sizeof(high_sco_entry))

#define HIGH_SCO_FILE_NAME "KLOCGA.SCO"

extern int load_high_scores(game_state * state);
extern int save_high_scores(game_state * state);

extern void draw_high_sco_window(game_state * state,
    const char * top_row_text, const char * bottom_row_text, int level_num);
extern int show_high_sco_name_entry_window(game_state * state);

// === Texts, game strings ===

#define HELP_PAGE_CONTROLS_MENU 0
#define HELP_PAGE_CONTROLS_GAME 1
#define HELP_PAGE_GAME 2
#define HELP_PAGE_ABOUT 3

extern const char * help_pages[NUM_HELP_PAGES];

extern const char * str_game_scr_moves;
extern const char * str_game_scr_level;
extern const char * str_game_help_key_1;
extern const char * str_game_help_key_2;
extern const char * str_confirm_restart;
extern const char * str_confirm_exit_main_menu;
extern const char * str_confirm_yes;
extern const char * str_confirm_no;
extern const char * str_text_help_page;
extern const char * str_text_help_keys;
extern const char * str_high_sco_level_row;
extern const char * str_high_sco_bottom_row_default;
extern const char * str_high_sco_top_row_level_won;
extern const char * str_high_sco_bottom_row_name_enter;
extern const char * str_err_new_game_failure;
extern const char * str_err_high_sco_load;
extern const char * str_err_high_sco_save;
extern const char * str_err_window_press_enter;
extern const char * str_err_mem_alloc;
extern const char * str_thanks;


// === Keyboard scan codes ===

#define SCAN_CODE_ESC 0x1
#define SCAN_CODE_BKSP 0xe
#define SCAN_CODE_ENTER 0x1c
#define SCAN_CODE_UP 0x48
#define SCAN_CODE_F1 0x3b
#define SCAN_CODE_F2 0x3c
#define SCAN_CODE_F3 0x3d
#define SCAN_CODE_F10 0x44
#define SCAN_CODE_LEFT 0x4b
#define SCAN_CODE_RIGHT 0x4d
#define SCAN_CODE_DOWN 0x50
#define SCAN_CODE_PGUP 0x49
#define SCAN_CODE_PGDN 0x51
#define SCAN_CODE_CTRL_LEFT 0x73
#define SCAN_CODE_CTRL_RIGHT 0x74
#define SCAN_CODE_W 0x11
#define SCAN_CODE_R 0x13
#define SCAN_CODE_A 0x1e
#define SCAN_CODE_S 0x1f
#define SCAN_CODE_D 0x20
#define SCAN_CODE_H 0x23

// === Shared inline asm funcs between modules ===

// wait_keypress_get_scancode: Calls BiOS interrupt "wait for keypress"
//   (int 16h, ah = 0h). Returns the scancode only (ah).
// (Note: could use bioskey(0) )
    
extern uchar wait_keypress_get_scancode(void);

#pragma aux wait_keypress_get_scancode = \
    "mov    ah, 0h" \
    "int    16h" \
    __value [ah] \
    __modify [al];

// wait_keypress: like  wait_keypress_get_scancode, but returns both
//   scancode (ah) and character input (al)
    
extern ushort wait_keypress(void);

#pragma aux wait_keypress = \
    "mov    ah, 0h" \
    "int    16h" \
    __value [ax];
    
// Mode constants for set_video_mode
    
#define TEXT_MODE_COLOR_80 0x3
#define GRAPH_MODE_CGA_320_200 0x4 
    
// set_video_mode: Set video mode using a BIOS interrupt
//   (int 10h, ah = 0h, al = mode)
    
extern void set_video_mode(uchar mode);

#pragma aux set_video_mode = \
    "xor    ah, ah" \
    "int    10h" \
    __parm [al] \
    __modify [ah];


#endif
