// KLOCGA program, by knt47

// KLCGAME.C: The rules of game & related routines, dialogs,
//   help and high scores windows

/*
  Copyright (c) 2022 knt47

  Redistribution and use in source and binary forms, with or without 
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "klchead.h"

void free_game_state_board_ptrs(game_state * state)
{
    if (state->board)
    {
        free(state->board); state->board = NULL;
    }
    
    // NOTE: this assumes that board and board_temp are acatually
    //   a single buffer
    if (state->board_temp)
        state->board_temp = NULL;
}

// --- Tile movement functions ---

#define BYTE_MAX_VALUE 0xff
#define BOARD_CHAR_ADDED_BORDER 0xff

int new_game(game_state * state, int level_number)
{
    int new_height, new_width, size;
    int num_blocks, x, y, k, board_i, board_in_i, block_arr_i;
    char * temp_buf;
    // char block_found_arr[SHAPE_FOUND_ARR_SIZE];
    char board_single;
    
    free_game_state_board_ptrs(state);
    
    if ((level_number < 0) || (level_number >= NUM_MAIN_MENU_ITEMS))
        return STATUS_FAILURE;
    
    // Note: only the "level" filed is checked in level_data
    
    if (!game_menu_levels[level_number].level)
        return STATUS_FAILURE;
    
    new_width = (int)(game_menu_levels[level_number].width);
    new_height = (int)(game_menu_levels[level_number].height);
    size = (new_width + 2) * (new_height + 2);
    
    temp_buf = (char *)malloc(size * 2);
    
    if (!temp_buf) 
        return STATUS_FAILURE;
    
    state->board = temp_buf;
    state->board_temp = temp_buf + size;
    
    memset(temp_buf, BOARD_CHAR_ADDED_BORDER, size);
    board_i = new_width + 3; board_in_i = 0;
    
    // Add an 1px border to state->board. This makes implementing "neighbors"
    //   in graphics code easier, but of course, increases size in memory
    
    for (k = 0; k < new_height; ++k)
    {
        memcpy(temp_buf + board_i, 
            game_menu_levels[level_number].level + board_in_i, new_width);
        board_i += new_width + 2; board_in_i += new_width;
    }
    
    // duplicate the first half of the buffer
    memcpy(temp_buf + size, temp_buf, size);
    
    num_blocks = 0; 
    board_i = new_width + 3;
    // Note: Setting cur_x and cur_y to initial value is not necessary
    //   as the function will fail if the "target block" is not found.
    state->target_block_index = -1;
    state->target_area_x = BYTE_MAX_VALUE;
    state->target_area_y = BYTE_MAX_VALUE;
    
    // Fill state->blocks array
    
    for (y = 0; y < new_height; ++y)
    {
        for (x = 0; x < new_width; ++x)
        {
            // board_single = temp_buf[y * new_width + x];
            // Faster without a multiply on 8088
            board_single = temp_buf[board_i++];
            
            if (board_single == '.')
            {
                if (x < state->target_area_x)
                    state->target_area_x = x;
                
                if (y < state->target_area_y)
                    state->target_area_y = y;
            }
            
            // space, '-', '.', are not considered "blocks" but '#',  '*'
            //   are. '#' cannot be moved unlike other blocks
            if ((board_single != ' ') && (board_single != '.') && 
                    (board_single != '-'))
            {
                block_arr_i = -1;
                
                // Look up board_single in blocks (TODO, "slow" linear search)
                for (k = 0; k < num_blocks; ++k)
                {
                    if (state->blocks[k].block_char == board_single)
                    {
                        block_arr_i = k; break;
                    }
                }
                
                if (block_arr_i < 0)
                {
                    block_arr_i = num_blocks++;
                    
                    if (num_blocks >= MAX_NUM_SHAPES)
                        return STATUS_FAILURE;
                    
                    state->blocks[block_arr_i].block_char = board_single;
                    state->blocks[block_arr_i].width = 1;
                    state->blocks[block_arr_i].height = 1;
                    state->blocks[block_arr_i].pos_x = (uchar)x;
                    state->blocks[block_arr_i].pos_y = (uchar)y;
                    
                    // Set cur_x and cur_y in state to the coordinates to the
                    //   first square found to belong to the '*' (target) block
                    //   (and not to its pos_x and pos_y which is the top left
                    //   corner of the bounding rect and not necessarily part 
                    //   of the block)
                    if (board_single == '*')
                    {
                        state->cur_x = x; state->cur_y = y;
                        state->target_block_index = block_arr_i;
                    }
                } else
                {
                    // Note: if the block has a concave shape, state->blocks
                    //   will store its bounding rectangle and not all squares
                    //   within the rectangle will belong to the block.
                    if (x < state->blocks[block_arr_i].pos_x)
                        state->blocks[block_arr_i].pos_x = (uchar)x;
                    
                    if (y < state->blocks[block_arr_i].pos_y)
                        state->blocks[block_arr_i].pos_y = (uchar)y;
                    
                    if ((x - (int)state->blocks[block_arr_i].pos_x + 1) >
                            (int)state->blocks[block_arr_i].width)
                        state->blocks[block_arr_i].width = (uchar)
                            (x - (int)state->blocks[block_arr_i].pos_x + 1);
                            
                    if ((y - (int)state->blocks[block_arr_i].pos_y + 1) >
                            (int)state->blocks[block_arr_i].height)
                        state->blocks[block_arr_i].height = (uchar)
                            (y - (int)state->blocks[block_arr_i].pos_y + 1);
                }
                
            }
        }
        
        board_i += 2;
    }
    
    if ((state->target_block_index < 0) || 
            (state->target_area_x == BYTE_MAX_VALUE) ||
            (state->target_area_y == BYTE_MAX_VALUE))
        return STATUS_FAILURE;
    
    state->num_blocks = num_blocks; state->level_num = level_number;
    state->width = new_width; state->height = new_height;
    state->size = size;
    state->num_moves = 0;
    state->game_won = BOOL_FALSE;

    // prev_moved_block will be '\0' if no block has been moved yet
    state->prev_moved_block = 0;
    
    // Set state_cur_* based on the position of the '*' block
    // TODO, this square may not be part of the block
    
    return STATUS_SUCCESS;
}

int move_board_temp(game_state * state, int new_pos_x, int new_pos_y,
    int pos_start_move, int block_arr_i, char block_ch)
{
    int pos_new = (new_pos_y + 1) * (state->width + 2) + new_pos_x + 1;
    int width = state->blocks[block_arr_i].width;
    int height = state->blocks[block_arr_i].height;
    int incr_end_row = state->width - width + 2;
    int x, y, board_i_new, board_i;
    char board_ch_new, board_ch;
    
    // Tries to perform a move, updates state->board_temp. Returns
    //   STATUS_FAILURE if the move was found to be illegal (e.g. collision)
    
    board_i_new = pos_start_move;
    
    // First, clear the block at its original position
    
    for (y = 0; y < height; ++y)
    {
        for (x = 0; x < width; ++x)
        {
            board_ch_new = state->board_temp[board_i_new];
            
            if (board_ch_new == block_ch)
            {
                state->board_temp[board_i_new] = ' ';
            } else
            {
                if (board_ch_new == (block_ch | 0x80))
                {
                    state->board_temp[board_i_new] = '.';
                } else
                {
                    if ((board_ch_new == 0x80) && (block_ch == '*'))
                        state->board_temp[board_i_new] = '-';
                }
            }
                
            ++board_i_new;
        }
        
        board_i_new += incr_end_row;
    }
    
    
    // Try to place the block to its new position. Return STATUS_FAILURE
    //   if a collision is detected
    board_i_new = pos_new; board_i = pos_start_move;
    
    for (y = 0; y < height; ++y)
    {
        for (x = 0; x < width; ++x)
        {
            board_ch = state->board[board_i];
            
            if ((board_ch == block_ch) || (board_ch == (block_ch | 0x80)) || 
                    ((block_ch == '*') && (board_ch == 0x80)))
            {
                board_ch_new = state->board_temp[board_i_new];
                

                switch (board_ch_new)
                {
                    case ' ':
                        state->board_temp[board_i_new] = block_ch; break;
                    case '.':
                        state->board_temp[board_i_new] = 
                            block_ch | 0x80; break;
                    case '-':
                    {
                        if (block_ch != '*')
                            return STATUS_FAILURE;
                        
                        state->board_temp[board_i_new] = 0x80; break;
                    }
                    default:
                        return STATUS_FAILURE;
                }
            }
                
            ++board_i_new; ++board_i;
        }
        
        board_i_new += incr_end_row;
        board_i += incr_end_row;
    }
    
    return STATUS_SUCCESS;
}

int move_block(game_state * state, int pos_x, int pos_y, int direction)
{
    int pos_start = (pos_y + 1) * (state->width + 2) + pos_x + 1;
    int new_pos_x, new_pos_y, pos_start_move, k, num_blocks, block_arr_i;
    int move_result;
    char block_ch;
    
    block_ch = state->board[pos_start] & 0x7f;
    
    // ' ', '.', '-', '#' cannot be moved
    switch (block_ch)
    {
        case ' ':
        case '.':
        case '-':
        case '#':
        case 0x7f:
            // 0x7f: was 0xff
            return STATUS_FAILURE;
            
        case 0:
            block_ch = '*';  break;  // was 0x80 
    }
        
    // Look up block_ch in blocks (TODO, "slow" linear search)
    block_arr_i = -1; num_blocks = state->num_blocks;
    
    for (k = 0; k < num_blocks; ++k)
    {
        if (state->blocks[k].block_char == block_ch)
        {
            block_arr_i = k; break;
        }
    }
    
    if (block_arr_i < 0)
        return STATUS_FAILURE;  // this should never happen (?)
        
    new_pos_x = (int)state->blocks[block_arr_i].pos_x; 
    new_pos_y = (int)state->blocks[block_arr_i].pos_y;
    pos_start_move = (new_pos_y + 1) * (state->width + 2) + new_pos_x + 1;
        
    switch (direction)
    {
        case DIRECTION_LEFT:
            --new_pos_x; break;
        case DIRECTION_UP:
            --new_pos_y; break;
        case DIRECTION_RIGHT:
            ++new_pos_x; break;
        case DIRECTION_DOWN:
            ++new_pos_y; break;
        default:
             return STATUS_FAILURE;
    }
    
    if ((new_pos_x < 0) || 
            ((state->blocks[block_arr_i].width + new_pos_x) > state->width))
        return STATUS_FAILURE;
    
    if ((new_pos_y < 0) || 
            ((state->blocks[block_arr_i].height + new_pos_y) > state->height))
        return STATUS_FAILURE;
    
    move_result = move_board_temp(state, new_pos_x, new_pos_y,
        pos_start_move, block_arr_i, block_ch);
    
    if (move_result)
    {
        // "accept" the move, copy board_temp to board
        memcpy(state->board, state->board_temp, state->size);
        
        // update pos_x and pos_y of block
        state->blocks[block_arr_i].pos_x = (uchar)new_pos_x;
        state->blocks[block_arr_i].pos_y = (uchar)new_pos_y;
        
        if (state->prev_moved_block != block_ch)
        {
            state->prev_moved_block = block_ch;
            ++state->num_moves;
        }
        
        if (block_ch == '*')
            state->game_won = (new_pos_x == (int)state->target_area_x) &&
                (new_pos_y == (int)state->target_area_y);
    } else
    {
        // Move was invalid, restore board_temp from board
        memcpy(state->board_temp, state->board, state->size);
    }
    
    return move_result;
}

// --- Game screen drawing routines ---

#define MOVE_NUM_PRINT_LIMIT 999

#define POS_X_BOXES_RIGHT 63
#define WIDTH_BOXES_RIGHT 16
#define POS_Y_MOVES_BOX 104
#define MOVES_BOX_HEIGHT 11
#define MOVES_TEXT_CHARS_NUM 3
#define POS_X_MOVES_PART2 (POS_X_BOXES_RIGHT + 9)
#define BOX_TEXT_COLORS_BLACK 0x300
#define BOX_TEXT_COLORS_MAGENTA 0x302
#define MOVES_TEXT_BUF_SIZE 4

void update_moves_box(game_state * state)
{
    // Note that this function does not call update_screen
    
    // HACK to avoid buffer overrun, unsigned compare
    
    char temp_buf[MOVES_TEXT_BUF_SIZE];
    ushort move_num = ((ushort)state->num_moves <= MOVE_NUM_PRINT_LIMIT) ? 
        (ushort)state->num_moves : MOVE_NUM_PRINT_LIMIT;
        
    sprintf(temp_buf, "% 3u", move_num);
    
    draw_white_rect_with_border(state->video_buf, 
        SCREEN_WIDTH_BYTES * POS_Y_MOVES_BOX + POS_X_BOXES_RIGHT, 
        WIDTH_BOXES_RIGHT, MOVES_BOX_HEIGHT);
    draw_text_6x8(state->video_buf, str_game_scr_moves, 
        SCREEN_WIDTH_BYTES * (POS_Y_MOVES_BOX + 2) + POS_X_BOXES_RIGHT + 1,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
    draw_text_6x8(state->video_buf, temp_buf, 
        SCREEN_WIDTH_BYTES * (POS_Y_MOVES_BOX + 2) + POS_X_MOVES_PART2 + 1,
        MOVES_TEXT_CHARS_NUM, BOX_TEXT_COLORS_BLACK);
}

#define POS_Y_HELP_KEY_BOX 172
#define HELP_KEY_BOX_HEIGHT 21

void game_screen_help_text(game_state * state)
{
    draw_white_rect_with_border(state->video_buf, 
        SCREEN_WIDTH_BYTES * POS_Y_HELP_KEY_BOX + POS_X_BOXES_RIGHT, 
        WIDTH_BOXES_RIGHT, HELP_KEY_BOX_HEIGHT);
    
    draw_text_6x8(state->video_buf, str_game_help_key_1, 
        SCREEN_WIDTH_BYTES * (POS_Y_HELP_KEY_BOX + 2) + 
            POS_X_BOXES_RIGHT + 1,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
    
    draw_text_6x8(state->video_buf, str_game_help_key_2, 
        SCREEN_WIDTH_BYTES * (POS_Y_HELP_KEY_BOX + 12) + 
            POS_X_BOXES_RIGHT + 1,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
}


#define POS_Y_LEVEL_NAME_BOX 72
#define LEVEL_NAME_BOX_HEIGHT_1 19
#define LEVEL_NAME_BOX_HEIGHT_2 27

void draw_game_screen(game_state * state)
{
    // Note that this function does not call update_screen
    int level_line_2_start = 0;
    int level_box_height = LEVEL_NAME_BOX_HEIGHT_1;
    int level_num = state->level_num;

    // TODO, hardocoded numbers
    draw_empty_game_screen(state->video_buf, 
        state->width * TILE_WIDTH_BYTES + ((state->width > 9) ? 3 : 6 ));
    
    // Show level name
    if (game_menu_levels[level_num].name_break)
    {
        level_line_2_start = (int)(
            game_menu_levels[level_num].name_break) + 1;
        level_box_height = LEVEL_NAME_BOX_HEIGHT_2;
    }
    
    draw_white_rect_with_border(state->video_buf, 
        SCREEN_WIDTH_BYTES * POS_Y_LEVEL_NAME_BOX + POS_X_BOXES_RIGHT, 
        WIDTH_BOXES_RIGHT, level_box_height);
    
    draw_text_6x8(state->video_buf, str_game_scr_level, 
        SCREEN_WIDTH_BYTES * (POS_Y_LEVEL_NAME_BOX + 2) + 
            POS_X_BOXES_RIGHT + 1,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
    
    if (level_line_2_start)
        draw_text_6x8(state->video_buf,
            game_menu_levels[level_num].level_name,
            SCREEN_WIDTH_BYTES * (POS_Y_LEVEL_NAME_BOX + 10) + 
                POS_X_BOXES_RIGHT + 1,
            level_line_2_start - 1, BOX_TEXT_COLORS_BLACK);
        
    draw_text_6x8(state->video_buf,
        game_menu_levels[level_num].level_name + level_line_2_start,
        SCREEN_WIDTH_BYTES * (POS_Y_LEVEL_NAME_BOX - 9 + level_box_height) +
            POS_X_BOXES_RIGHT + 1, DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
    
    game_screen_help_text(state);
        
    // Draw board
    draw_board(state, 0, 0, DRAW_BOARD_FULL, DRAW_BOARD_FULL);
    update_moves_box(state);
    
    // TODO, add additional items
}


int move_block_update_screen(game_state * state, int direction)
{
    // Returns STATUS_SUCCESS if the move was valid, STATUS_FAILURE otherwise
    // On success, moves cursor, re-draws the screen and also calls 
    //   update_screen
    int move_result; 
    
    if ((state->game_won) || (state->num_moves >= MOVE_NUM_PRINT_LIMIT))
        return STATUS_FAILURE;
    
    move_result = move_block(state, state->cur_x, state->cur_y, direction);
    
    if (move_result)
    {
        switch (direction)
        {
            case DIRECTION_LEFT:
                --state->cur_x; break;
            case DIRECTION_UP:
                --state->cur_y; break;
            case DIRECTION_RIGHT:
                ++state->cur_x; break;
            case DIRECTION_DOWN:
                ++state->cur_y; break;
                // Note: default case unnecessary as move_block would fail
                // Also, no range check for the same reason
        }
        
        // Currently, re-draws all blocks, should be optimized
        draw_board(state, 0, 0, DRAW_BOARD_FULL, DRAW_BOARD_FULL);
        update_moves_box(state);
        update_screen(state);
    }
    
    return move_result;
}

int move_cursor_update_screen(game_state * state, int direction)
{
    // Returns STATUS_SUCCESS if the cursor move was valid, STATUS_FAILURE
    //    otherwise. On success, re-draws the affected squares of board 
    //    and calls update_screen.
    
    switch (direction)
    {
        case DIRECTION_LEFT:
        {
            if (state->cur_x <= 0)
                return STATUS_FAILURE;
            
            --state->cur_x; 
            draw_board(state, state->cur_x, state->cur_y,
                state->cur_x + 2, state->cur_y + 1);
            break;
        }
        case DIRECTION_UP:
        {
            if (state->cur_y <= 0)
                return STATUS_FAILURE;
            
            --state->cur_y;
            draw_board(state, state->cur_x, state->cur_y,
                state->cur_x + 1, state->cur_y + 2);
            break;
        }
        case DIRECTION_RIGHT:
        {
            if (state->cur_x >= (state->width - 1))
                return STATUS_FAILURE;
            
            ++state->cur_x; 
            draw_board(state, state->cur_x - 1, state->cur_y,
                state->cur_x + 1, state->cur_y + 1);
            break;
        }
        case DIRECTION_DOWN:
        {
            if (state->cur_y >= (state->height - 1))
                return STATUS_FAILURE;
            
            ++state->cur_y; 
            draw_board(state, state->cur_x, state->cur_y - 1,
                state->cur_x + 1, state->cur_y + 1);
            break;
        }
            
        default:
            return STATUS_FAILURE;
    }
    
    update_screen(state);
    return STATUS_SUCCESS;
}

// --- "Dialog boxes" ---

// Note: x coordinate, width in bytes, not pixels
#define CONFIRM_DLG_POS_X 26
#define CONFIRM_DLG_POS_Y 80
#define CONFIRM_DLG_WIDTH 34
#define CONFIRM_DLG_HEIGHT 40
#define CONFIRM_DLG_TEXT_POS_X 28
#define CONFIRM_DLG_TEXT_POS_Y_1 84
#define CONFIRM_DLG_TEXT_POS_Y_2 94
#define CONFIRM_DLG_TEXT_POS_X_YES 34
#define CONFIRM_DLG_TEXT_POS_X_NO 46
#define CONFIRM_DLG_TEXT_POS_Y_YES_NO 108
#define BOX_TEXT_COLORS_CYAN_BACKGR 0x100

int confirm_dialog(game_state * state, const char * row_1_text,
    const char * row_2_text)
{
    int result = BOOL_TRUE;
    int draw_yes_no = BOOL_TRUE;
    
    draw_white_rect_with_border(state->video_buf, 
        SCREEN_WIDTH_BYTES * CONFIRM_DLG_POS_Y + CONFIRM_DLG_POS_X, 
        CONFIRM_DLG_WIDTH, CONFIRM_DLG_HEIGHT);
    
    draw_text_6x8(state->video_buf, row_1_text, 
        SCREEN_WIDTH_BYTES * CONFIRM_DLG_TEXT_POS_Y_1 + 
            CONFIRM_DLG_TEXT_POS_X,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
    
    if (row_2_text)
    {
        draw_text_6x8(state->video_buf, row_2_text, 
            SCREEN_WIDTH_BYTES * CONFIRM_DLG_TEXT_POS_Y_2 + 
                CONFIRM_DLG_TEXT_POS_X,
            DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
    }
    
    for (;;)
    {
        if (draw_yes_no)
        {
            draw_text_6x8(state->video_buf, str_confirm_yes, 
                SCREEN_WIDTH_BYTES * CONFIRM_DLG_TEXT_POS_Y_YES_NO + 
                    CONFIRM_DLG_TEXT_POS_X_YES, DRAW_TEXT_MAX_LEN, 
                result ? BOX_TEXT_COLORS_CYAN_BACKGR : BOX_TEXT_COLORS_BLACK);
            
            draw_text_6x8(state->video_buf, str_confirm_no, 
                SCREEN_WIDTH_BYTES * CONFIRM_DLG_TEXT_POS_Y_YES_NO + 
                    CONFIRM_DLG_TEXT_POS_X_NO, DRAW_TEXT_MAX_LEN, 
                result ? BOX_TEXT_COLORS_BLACK : BOX_TEXT_COLORS_CYAN_BACKGR);
            
            update_screen(state);
            draw_yes_no = BOOL_FALSE;
        }
        
        switch (wait_keypress_get_scancode())
        {
            case SCAN_CODE_ENTER:
                return result;
            
            case SCAN_CODE_ESC:
                return BOOL_FALSE;
                
            case SCAN_CODE_LEFT:
            {
                if (!result)
                {
                    result = BOOL_TRUE; draw_yes_no = BOOL_TRUE;
                }
                break;
            }
            
            case SCAN_CODE_RIGHT:
            {
                if (result)
                {
                    result = BOOL_FALSE; draw_yes_no = BOOL_TRUE;
                }
                break;
            }
        }
    }
    
}

// --- Help window ---

// Note: x coordinete, width in bytes, not pixels

#define HELP_WINDOW_POS_X 4
#define HELP_WINDOW_POS_Y 12
#define HELP_WINDOW_WIDTH 56
#define HELP_WINDOW_HEIGHT 176
#define HELP_WINDOW_TEXT_POS_X 6
#define HELP_WINDOW_HELP_TEXT_POS_Y 20
#define HELP_WINDOW_TEXT_POS_X_PAGE 40
#define HELP_WINDOW_TEXT_POS_Y_PAGE 166
#define HELP_WINDOW_TEXT_POS_Y_KEYS 176
#define HELP_PAGE_NUM_BUF_SIZE 8

void show_help(game_state * state, ushort page_num_start)
{
    int page_num = (int)page_num_start;
    int redraw = BOOL_TRUE;
    // NOTE: Buffer overrun may occur if the number of
    //   help pages is greater than 999
    char temp_buf[HELP_PAGE_NUM_BUF_SIZE];
    
    // unsigned, no need to check for negative numbers
    if (page_num_start >= NUM_HELP_PAGES)
        return;
    
    for (;;)
    {
        if (redraw)
        {
            // Redraw entire window on moving to another page
            // As most text changes, probably it is not worth optimizing
            draw_white_rect_with_border(state->video_buf, 
                SCREEN_WIDTH_BYTES * HELP_WINDOW_POS_Y + HELP_WINDOW_POS_X, 
                HELP_WINDOW_WIDTH, HELP_WINDOW_HEIGHT);
            
            draw_multi_line_text(state->video_buf, help_pages[page_num],
                SCREEN_WIDTH_BYTES * HELP_WINDOW_HELP_TEXT_POS_Y +
                    HELP_WINDOW_TEXT_POS_X, BOX_TEXT_COLORS_BLACK);

            draw_text_6x8(state->video_buf, str_text_help_page, 
                SCREEN_WIDTH_BYTES * HELP_WINDOW_TEXT_POS_Y_PAGE + 
                    HELP_WINDOW_TEXT_POS_X,
                DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
            
            draw_text_6x8(state->video_buf, str_text_help_keys, 
                SCREEN_WIDTH_BYTES * HELP_WINDOW_TEXT_POS_Y_KEYS + 
                    HELP_WINDOW_TEXT_POS_X,
                DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
            
            sprintf(temp_buf, "%d/%d", page_num + 1, NUM_HELP_PAGES);
            
            draw_text_6x8(state->video_buf, temp_buf, 
                SCREEN_WIDTH_BYTES * HELP_WINDOW_TEXT_POS_Y_PAGE + 
                    HELP_WINDOW_TEXT_POS_X_PAGE,
                DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
            
            update_screen(state);
            redraw = BOOL_FALSE;
        }
        
        switch (wait_keypress_get_scancode())
        {
            case SCAN_CODE_ESC:
                return;
                
            case SCAN_CODE_LEFT:
            {
                if (page_num > 0)
                {
                    --page_num; redraw = BOOL_TRUE;
                }
                break;
            }
            
            case SCAN_CODE_RIGHT:
            {
                if (page_num < (NUM_HELP_PAGES - 1))
                {
                    ++page_num; redraw = BOOL_TRUE;
                }
                break;
            }
        }
    }
}

// --- High scores ---

int load_high_scores(game_state * state)
{
    FILE * high_sco_file = fopen(HIGH_SCO_FILE_NAME, "rb");
    int result;
    
    if (!high_sco_file)
        return STATUS_FAILURE;
    
    result = fread(state->high_scores, 1, 
        HIGH_SCO_DATA_SIZE, high_sco_file) == HIGH_SCO_DATA_SIZE;
        
    fclose(high_sco_file);
    return result;
}

int save_high_scores(game_state * state)
{
    FILE * high_sco_file = fopen(HIGH_SCO_FILE_NAME, "wb");
    int result;
    
    if (!high_sco_file)
        return STATUS_FAILURE;
    
    result = fwrite(state->high_scores, 1, 
        HIGH_SCO_DATA_SIZE, high_sco_file) == HIGH_SCO_DATA_SIZE;
        
    fclose(high_sco_file);
    return result;
}

#define HIGH_SCO_WINDOW_POS_X 4
#define HIGH_SCO_WINDOW_POS_Y 20
#define HIGH_SCO_WINDOW_WIDTH 56
#define HIGH_SCO_WINDOW_HEIGHT 160
#define HIGH_SCO_WINDOW_TEXT_POS_X 6
#define HIGH_SCO_WINDOW_ROW_1_POS_Y 24
#define HIGH_SCO_WINDOW_ROW_2_POS_Y 36
#define HIGH_SCO_WINDOW_ROW_LAST_POS_Y 168
#define HIGH_SCO_REL_POS_X_LEVEL 28
#define HIGH_SCO_WINDOW_SCORES_POS_X 12
#define HIGH_SCO_WINDOW_SCORES_POS_Y 56
#define HIGH_SCO_WINDOW_REL_POS_NAME 6
#define HIGH_SCO_WINDOW_REL_POS_MOVES 34
#define HIGH_SCO_WINDOW_SCORES_DISTANCE_Y 16

#define HIGH_SCO_SCREEN_POS_1 (SCREEN_WIDTH_BYTES * \
    HIGH_SCO_WINDOW_ROW_1_POS_Y + HIGH_SCO_WINDOW_TEXT_POS_X)
#define HIGH_SCO_SCREEN_POS_2 (SCREEN_WIDTH_BYTES * \
    HIGH_SCO_WINDOW_ROW_2_POS_Y + HIGH_SCO_WINDOW_TEXT_POS_X)

void draw_high_sco_window(game_state * state, const char * top_row_text,
    const char * bottom_row_text, int level_num)
{
    int high_sco_index = (int)(game_menu_levels[level_num].high_scores_index);
    high_sco_entry * high_sco_start;
    char temp_buf[MOVES_TEXT_BUF_SIZE];
    int entry_i;
    ushort scr_offset;
    
    if (high_sco_index >= MENU_SPECIAL_FIRST)
        return;
    
    high_sco_start = state->high_scores + 
        (high_sco_index * HIGH_SCO_ENTRIES_PER_LEVEL);
        
    draw_white_rect_with_border(state->video_buf, 
        SCREEN_WIDTH_BYTES * HIGH_SCO_WINDOW_POS_Y + HIGH_SCO_WINDOW_POS_X, 
        HIGH_SCO_WINDOW_WIDTH, HIGH_SCO_WINDOW_HEIGHT);
    
    if (top_row_text)
        draw_text_6x8(state->video_buf, top_row_text, 
            HIGH_SCO_SCREEN_POS_1, DRAW_TEXT_MAX_LEN,
            BOX_TEXT_COLORS_MAGENTA);

    draw_text_6x8(state->video_buf, str_high_sco_level_row, 
        top_row_text ? HIGH_SCO_SCREEN_POS_2 : HIGH_SCO_SCREEN_POS_1,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);

    draw_text_6x8(state->video_buf, game_menu_levels[level_num].level_name, 
        top_row_text ? (HIGH_SCO_SCREEN_POS_2 + HIGH_SCO_REL_POS_X_LEVEL) : 
            (HIGH_SCO_SCREEN_POS_1 + HIGH_SCO_REL_POS_X_LEVEL),
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
    
    draw_text_6x8(state->video_buf,
        bottom_row_text ? bottom_row_text : str_high_sco_bottom_row_default,
        SCREEN_WIDTH_BYTES * HIGH_SCO_WINDOW_ROW_LAST_POS_Y +
            HIGH_SCO_WINDOW_TEXT_POS_X, 
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
    
    scr_offset = SCREEN_WIDTH_BYTES * HIGH_SCO_WINDOW_SCORES_POS_Y + 
        HIGH_SCO_WINDOW_SCORES_POS_X;
    
    for (entry_i = 0; entry_i < HIGH_SCO_ENTRIES_PER_LEVEL; ++entry_i)
    {
        if ((high_sco_start[entry_i].num_moves > 0) &&
                (high_sco_start[entry_i].num_moves <= MOVE_NUM_PRINT_LIMIT))
        {
            temp_buf[0] = (char)(entry_i + '1');
            
            draw_text_6x8(state->video_buf, temp_buf, scr_offset,
                1, BOX_TEXT_COLORS_BLACK);
            
            draw_text_6x8(state->video_buf,
                high_sco_start[entry_i].player_name,
                scr_offset + HIGH_SCO_WINDOW_REL_POS_NAME,
                DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
            
            sprintf(temp_buf, "% 3u", high_sco_start[entry_i].num_moves);
            
            draw_text_6x8(state->video_buf, temp_buf,
                scr_offset + HIGH_SCO_WINDOW_REL_POS_MOVES,
                DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_BLACK);
        }
        
        scr_offset += SCREEN_WIDTH_BYTES * HIGH_SCO_WINDOW_SCORES_DISTANCE_Y;
    }
}

#define NAME_PLACEHOLDER_CHAR 0x84
#define HIGH_SCO_MAX_NUM_CHARS (HIGH_SCO_NAME_SIZE - 1)
// NOTE: Currently, add_high_sco_entry_show_window allows only ASCII 
//   characters 0x20 - 0x7e in name
#define HIGH_SCO_NAME_ALLOWED_CHAR_MIN 0x20
#define HIGH_SCO_NAME_ALLOWED_CHAR_MAX 0x7e

int show_high_sco_name_entry_window(game_state * state)
{
    // This function must be called once after the level is completed.
    // Returns BOOL_TRUE if the user was prompted for a name and the number
    //   of moves was added to the high scores table, BOOL_FALSE otherwise.
    
    int high_sco_index = (int)(game_menu_levels[
        state->level_num].high_scores_index);
    high_sco_entry * high_sco_start = state->high_scores + 
        (high_sco_index * HIGH_SCO_ENTRIES_PER_LEVEL);
    int entry_i;
    int entry_add_pos = -1;
    int redraw;
    ushort scr_offset;
    ushort key_input;
    uchar key_input_low;
    int temp_buf_chars;
    char temp_buf[HIGH_SCO_MAX_NUM_CHARS];
    
    for (entry_i = 0; entry_i < HIGH_SCO_ENTRIES_PER_LEVEL; ++entry_i)
    {
        if ((high_sco_start[entry_i].num_moves > (ushort)state->num_moves) ||
                (high_sco_start[entry_i].num_moves == 0))
        {
            entry_add_pos = entry_i; break;
        }
    }
    
    if (entry_add_pos < 0)
    {
        // Number of moves is too high to be added to the high scores
        //   board. Show high scores, wait for ESC key and return BOOL_FALSE
        draw_high_sco_window(state, str_high_sco_top_row_level_won,
            NULL, state->level_num);
        update_screen(state);
    
        while (wait_keypress_get_scancode() != SCAN_CODE_ESC) ;

        return BOOL_FALSE;
    }
    
    if (entry_add_pos < (HIGH_SCO_ENTRIES_PER_LEVEL - 1))
        memmove(high_sco_start + (entry_add_pos + 1), 
            high_sco_start + entry_add_pos,
            ((HIGH_SCO_ENTRIES_PER_LEVEL - 1) - entry_add_pos) *
                sizeof(high_sco_entry));
        
    memset(&(high_sco_start[entry_add_pos].player_name),
        0, HIGH_SCO_NAME_SIZE);
    high_sco_start[entry_add_pos].num_moves = (ushort)state->num_moves;
    
    // TODO, this duplicates (in part) draw_high_sco_window
    
    draw_high_sco_window(state, str_high_sco_top_row_level_won,
        str_high_sco_bottom_row_name_enter, state->level_num);
    
    scr_offset = SCREEN_WIDTH_BYTES * (HIGH_SCO_WINDOW_SCORES_POS_Y + 
        HIGH_SCO_WINDOW_SCORES_DISTANCE_Y * entry_add_pos) + 
        HIGH_SCO_WINDOW_SCORES_POS_X;
        
    temp_buf[0] = (char)(entry_add_pos + '1');
            
    draw_text_6x8(state->video_buf, temp_buf, scr_offset,
        1, BOX_TEXT_COLORS_MAGENTA);
            
    sprintf(temp_buf, "% 3u", (ushort)state->num_moves);
    
    draw_text_6x8(state->video_buf, temp_buf,
        scr_offset + HIGH_SCO_WINDOW_REL_POS_MOVES,
        DRAW_TEXT_MAX_LEN, BOX_TEXT_COLORS_MAGENTA);
    
    scr_offset += HIGH_SCO_WINDOW_REL_POS_NAME;
    temp_buf_chars = 0;
    redraw = BOOL_TRUE;
    memset(temp_buf, NAME_PLACEHOLDER_CHAR, HIGH_SCO_MAX_NUM_CHARS);
    
    for (;;)
    {
        if (redraw)
        {
            draw_text_6x8(state->video_buf, temp_buf, scr_offset,
                HIGH_SCO_MAX_NUM_CHARS, BOX_TEXT_COLORS_MAGENTA);
            update_screen(state);
            redraw = BOOL_FALSE;
        }
        
        key_input = wait_keypress();
        key_input_low = (uchar)(key_input & 0xff);
        
        if ((key_input_low >= HIGH_SCO_NAME_ALLOWED_CHAR_MIN) && 
                (key_input_low <= HIGH_SCO_NAME_ALLOWED_CHAR_MAX))
        {
            if (temp_buf_chars < HIGH_SCO_MAX_NUM_CHARS)
            {
                temp_buf[temp_buf_chars++] = (char)key_input_low;
                redraw = BOOL_TRUE;
            }
        } else
        {
            switch (key_input >> 8)  // scancode
            {
                case SCAN_CODE_BKSP:
                {
                    if (temp_buf_chars > 0)
                    {
                        temp_buf[--temp_buf_chars] = 
                            (char)NAME_PLACEHOLDER_CHAR;
                        redraw = BOOL_TRUE;
                    }
                    
                    break;
                }
                
                case SCAN_CODE_ENTER:
                {
                    memcpy(&(high_sco_start[entry_add_pos].player_name),
                        temp_buf, temp_buf_chars);
                    high_sco_start[entry_add_pos].player_name[
                        temp_buf_chars] = 0;
                    return BOOL_TRUE;
                }
            }
        }
    }
}

