# KLOCGA "retro" game

KLOCGA is an implemantation of the puzzle game [Klotski](https://en.wikipedia.org/wiki/Klotski) for "IBM PC compatibles" running DOS using CGA graphics. It was written in C and can be built using the Open Watcom compiler (versions 1.9 or 2.0). 

![KLOCGA screenshot](img/klocga.png "KLOCGA screenshot")

*Screenshot of KLOCGA running in DOSBox*

## Features

- CGA graphics (supports EGA, VGA)
- 8086/8088 compatible
- 24 levels
- High scores
- In-game help
- A level number can be specified as a DOS command line argument.

## Controls (keyboard)

The program can be controlled using the keyboard.

- **Up**/**Down**/**Left**/**Right** arrows: Move cursor (+ shape)
- **WASD** or **Shift**+**Up**/**Down**/**Left**/**Right**: Move block
- **R**: Restart same level
- **F1**: In-game Help
- **Esc**: Exit to main menu

See more information in the in-game help (found in the [klctxten.c](/knt47/klocga/src/branch/main/src/klctxten.c) file.)

## Build instructions

A few environment variables must be set for the Watcom compiler and build tools. The "tools" subdirectory of the repository contains an example CMD script ans shell script file for setting up the environment variables on Windows and Linux, respectively. See also the readme.txt file of the compiler for more information on this.

The program can be built by running WMAKE. There are two different Makefiles, klcwin.mk for Windows (and DOS) and klclinux.mk for Linux. The OBJ subdirectory must be the working directory foe WMAKE. 

    wmake -f ../klcwin.mk
    wmake -f ../klclinux.mk

The executable file will be placed along with the objects files, into the OBJ subdirectory.

## License

- Most of KLOCGA source code is released under the 2-clause BSD license. See [src/COPYING.BSD](/knt47/klocga/src/branch/main/src/COPYING.BSD)
- The tileset and the game logo is also covered by the 2-clause BSD license. See the ["tiles" subdirectory](/knt47/klocga/src/branch/main/tiles) and the [src/klctiles.c](/knt47/klocga/src/branch/main/src/klctiles.c) file.
- The levels are from [GNOME Klotski](https://gitlab.gnome.org/GNOME/gnome-klotski), by Robert Ancell and other contributors, released under the GNU General Public License, version 3 or later. See [src/COPYING.GPL](/knt47/klocga/src/branch/main/src/COPYING.GPL) and the [src/klcleven.c](/knt47/klocga/src/branch/main/src/klcleven.c) file.
- The 6x8 font is based in a font by Alex Zorg who released it under the 3-clause BSD license. See the ["font_6x8" subdirectory](/knt47/klocga/src/branch/main/font_6x8) and the [src/klctiles.c](/knt47/klocga/src/branch/main/src/klctiles.c) file.

## Notes

I plan a complete rewrite of this, entirely in assembly language. C was initially chosen for the re-usability of the code responsible for moving the blocks.
 
