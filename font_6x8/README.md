# 6x8 font

This directory contains a fork of a 6x8 font created by Alex Zorg, originally uploaded to the the following repository: [Github repository](https://github.com/azorg/font_6x8). The original font uses the KOI8-R code page, but the KLOCGA game uses only the "ASCII-compatible" part (0x20 .. 0x7e). The original Python file (font_6x8_src.py, the fork is named font_6x8_src_forked.py) was modified so that it is able to convert the characters directly into the format used in the KLOCGA source code. The shape of one character, small letter "g" was also slightly modified.

## Instructions for conversion of the font

The font_6x8_src_forked.py script prints the character data to the standard output in the format that can be copied directly to the klcfon68.c file. Redirecting the output of the script to a file:

    python3 font_6x8_src_forked.py > font_6x8_out.txt

The klcfon68.c file must be updated manually. Note that this file also contains some symbols I have created manually without using this script.
    
## License

The fork (like the original) is released under the 3-clause BSD license, see COPYING.
