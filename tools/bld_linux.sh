#/bin/sh

# Exit on error
set -e

# *** ***  Paths that need to be modified  *** ***

# The full path to the "src" directory in the downloaded source code
export OUTSRCDIR="/home/example/sources/klocga/src"

# The directory where Open Watcom was installed
export WATCOMDIR="/home/example/software/ow20"

# Change WATCOMPLATFORM to binl to use 32-bit Watcom
export WATCOMPLATFORM=binl64

# *** *** *** *** ***

export PATH="$WATCOMDIR"/"$WATCOMPLATFORM":$PATH
export INCLUDE="$WATCOMDIR"/h
export WATCOM="$WATCOMDIR"
export EDPATH="$WATCOM"/eddat

export OUTDIR="$OUTSRCDIR"/OBJ

# *** *** Build *** ***

cd "$OUTDIR"

wmake -f ../klclinux.mk
