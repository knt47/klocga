@echo off

rem *** ***  Paths that need to be modified  *** ***

rem The drive containing the source code

set OUTDRIVE=D:

rem The full path to the "src" directory in the downloaded source code
rem    (With the drive substituted with %OUTDRIVE%)

set OUTSRCDIR=%OUTDRIVE%\sources\klocga\src

rem The directory where Open Watcom was installed

set WATCOMDIR=D:\software\watcom_win64

rem Change WATCOMPLATFORM to binnt to use 32-bit Watcom

set WATCOMPLATFORM=binnt64

rem *** *** *** *** ***

path %WATCOMDIR%\%$WATCOMPLATFORM%;%PATH%
set INCLUDE=%WATCOMDIR%\h
set WATCOM=%WATCOMDIR%
set EDPATH=%WATCOMDIR%\eddat
set WWINHELP=%WATCOMDIR%\binw

set OUTDIR=%OUTSRCDIR%\OBJ


rem *** *** Build *** ***

%OUTDRIVE%
cd %OUTDIR%

wmake -f ../klcwin.mk
